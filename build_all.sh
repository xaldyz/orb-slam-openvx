#!/bin/bash

function mkdir2()
{
    if [ ! -d "$1" ]; then
        mkdir -p $1
    fi
}

mkdir2 Thirdparty/g2o/build
cd Thirdparty/g2o/build
cmake -DCMAKE_BUILD_TYPE=Release -DG2O_USE_OPENMP=OFF -DG2O_EIGEN_OPENMP=OFF .. && make -j5
cd -

mkdir2 Thirdparty/g2o/build_openmp
cd Thirdparty/g2o/build_openmp
cmake -DCMAKE_BUILD_TYPE=Release -DG2O_USE_OPENMP=ON -DG2O_EIGEN_OPENMP=ON .. && make -j5
cd -

mkdir2 Thirdparty/DBoW2/build
cd Thirdparty/DBoW2/build
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 Thirdparty/DBoW2/build_tegracv
cd Thirdparty/DBoW2/build_tegracv
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_TEGRA_CV=ON .. && make -j5
cd -

mkdir2 build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=OFF -DUSE_G2O_OPENMP=OFF -DUSE_CUDA_ORB=OFF -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 build_mp
cd build_mp
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=OFF -DUSE_G2O_OPENMP=ON -DUSE_CUDA_ORB=OFF -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 build_vx
cd build_vx
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=ON -DUSE_G2O_OPENMP=OFF -DUSE_CUDA_ORB=OFF -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 build_vxmp
cd build_vxmp
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=ON -DUSE_G2O_OPENMP=ON -DUSE_CUDA_ORB=OFF -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 build_vxcuda
cd build_vxcuda
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=ON -DUSE_G2O_OPENMP=OFF -DUSE_CUDA_ORB=ON -DUSE_TEGRA_CV=OFF .. && make -j5
cd -

mkdir2 build_vxmpcuda
cd build_vxmpcuda
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_OPENVX=ON -DUSE_G2O_OPENMP=ON -DUSE_CUDA_ORB=ON -DUSE_TEGRA_CV=OFF .. && make -j5
cd -


mkdir2 script_exploration
ln -s ../bin/Release script_exploration/00standard
ln -s ../bin/Release_openmp script_exploration/01mp
ln -s ../bin/Release_openvx script_exploration/02vx
ln -s ../bin/Release_openvx_openmp script_exploration/03vxmp
ln -s ../bin/Release_openvx_cuda script_exploration/04vxcuda
ln -s ../bin/Release_openvx_openmp_cuda script_exploration/05vxmpcuda

tar xzf Vocabulary/ORBvoc.txt.tar.gz -C Vocabulary
