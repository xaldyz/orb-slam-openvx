# ORB-SLAM2 with OpenVX

This repository is an extension of the ORB-SLAM2 package to provide various parallel languages to the program.

To get all working, beside manual install of the libraries stated in the original ORB-SLAM2 package, a file named `build_all.sh` will build all the necessary steps
```
cd orb-slam-openvx
chmod +x build_all.sh
./build_all.sh
```

This will create **libORB_SLAM2.so**  at *lib* folder and the executables **mono_kitti** and **mono_euroc** in *bin* folder.

At the moment, only the monocular part is implemented.
