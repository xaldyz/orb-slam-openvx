/*------------------------------------------------------------------------------
Copyright © 2016 by Nicola Bombieri

XLib is provided under the terms of The MIT License (MIT):

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/
/**
 * @author Federico Busato, Michele Scala
 * Univerity of Verona, Dept. of Computer Science
 * federico.busato@univr.it
 */
#include <exception>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <limits>
#include <numeric>
#include <cmath>
#include <fstream>

#include "powermon.hpp"

PowermonInternal::PowermonInternal() : Powermon(NULL)/*,
                                     power_reader_40_0(40,0), 
                                     power_reader_40_1(40,1), 
                                     power_reader_40_2(40,2), 
                                     power_reader_41_0(41,0), 
                                     power_reader_41_1(41,1), 
                                     power_reader_41_2(41,2), 
                                     power_reader_42_0(42,0), 
                                     power_reader_42_1(42,1), 
                                     power_reader_42_2(42,2), 
                                     power_reader_43_0(43,0), 
                                     power_reader_43_1(43,1), 
                                     power_reader_43_2(43,2)//*/
                                     {
    for(int i = 0; i < 12; ++i)
    {
        power_reader[i] = new PowerReader(i/3+40, i % 3);
    }
	
}

PowermonInternal::PowermonInternal(std::ofstream* _log) :
                        Powermon(_log)/*, power_reader_40_0(40,0), 
                                     power_reader_40_1(40,1), 
                                     power_reader_40_2(40,2), 
                                     power_reader_41_0(41,0), 
                                     power_reader_41_1(41,1), 
                                     power_reader_41_2(41,2), 
                                     power_reader_42_0(42,0), 
                                     power_reader_42_1(42,1), 
                                     power_reader_42_2(42,2), 
                                     power_reader_43_0(43,0), 
                                     power_reader_43_1(43,1), 
                                     power_reader_43_2(43,2)//*/
                                     {
    for(int i = 0; i < 12; ++i)
    {
        power_reader[i] = new PowerReader(i/3+40, i % 3);
    }

}

PowermonInternal::~PowermonInternal() {
    
}

//------------------------------------------------------------------------------

bool PowermonInternal::start() {
    return start(std::chrono::steady_clock::now());
}
bool PowermonInternal::start(std::chrono::time_point<std::chrono::steady_clock> t ) {
    if (power_reader[0]->valid()) {
        this->powermon_found = true;
        this->power_max = 0;
        this->power_total = 0;
        this->sampled_instants = 0;
        this->stop_task = false;

        this->reset();
        
        ////avoid first read
        //mutex.lock();
        
        supportThread = new std::thread(PowermonInternal::task, this);
        
        return true && Powermon::start(t);
    } else {
        this->power_max = NAN;
        this->power_total = NAN;
        this->sampled_instants = static_cast<int>(NAN);
        std::cerr << "Not able to connect to PowermonInternal" << std::endl;
        
        return false;
    }
}



bool PowermonInternal::stop() {
    if (!powermon_found)
        return true;
    stop_task = true;
    mutex.unlock();
    (*supportThread).join();
    delete supportThread;

    this->reset();
    
    return false;
}

//------------------------------------------------------------------------------

void PowermonInternal::task(PowermonInternal* master){
	//PowermonInternal* master = (PowermonInternal*) ptr;
    master->mutex.lock();
    while(!master->stop_task)
    {
        master->mutex.unlock();
        //std::cout << "Starting read" << std::endl << std::flush;
        double watts = 0;
        std::vector<std::thread> workers;
        for (int i = 0; i < 12; i++) {
            /*workers.push_back(std::thread([master, i]() 
            {
                //std::cout << "Read " << i << " 0 start" << std::endl << std::flush;
                master->last_power_read[i].voltage = master->power_reader[i]->read(0);
                //std::cout << "Read " << i << " 0 finish" << std::endl << std::flush;
            }));
            workers.push_back(std::thread([master, i]() 
            {
                //std::cout << "Read " << i << " 1 start" << std::endl << std::flush;
                master->last_power_read[i].current = master->power_reader[i]->read(1);
                //std::cout << "Read " << i << " 1 finish" << std::endl << std::flush;
            }));*/
            workers.push_back(std::thread([master, i]() 
            {
                //std::cout << "Read " << i << " 2 start" << std::endl << std::flush;
                master->last_power_read[i].power = master->power_reader[i]->read(2);
                //std::cout << "Read " << i << " 2 finish" << std::endl << std::flush;
            }));
        }
        //std::cout << "END THREAD " << workers.size() << std::endl << std::flush;
        std::for_each(workers.begin(), workers.end(), [](std::thread &t) 
        {
            t.join();
        });

        watts = 0;
        for(int i = 0; i < 12; ++i) watts += master->last_power_read[i].power;
        
        master->power_total += watts;
        if (watts > master->power_max)
            master->power_max = watts;

        ++master->sampled_instants;
        
        master->log();
        master->mutex.lock();
    }
    master->mutex.unlock();
}


namespace std{
template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;

  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}
}

PowerRead::PowerRead(const char *c)
{
	is_valid = true;
	fd = open(c, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		is_valid = false;
	}
}
PowerRead::PowerRead(std::string s) : PowerRead(s.c_str()) {}
	
double PowerRead::read_value()
{
	char buf[33];
    lseek(fd, 0, 0);
    int n = read(fd, buf, 32);
    double value;
    if (n > 0) {
        buf[n] = 0;
        char *o = NULL;
        //value = strtod(buf, &o);
        value = strtod(buf,&o);
        value = 0.001*value;
    } else value = -1;
    return value;
}

bool PowerRead::valid() {
	return is_valid;
}

PowerReader::PowerReader(int number_sensor, int rail)
	:
		drv { PowerRead(std::string("/sys/devices/3160000.i2c/i2c-0/0-00") + std::to_string(number_sensor) + std::string("/iio_device/in_voltage") + std::to_string(rail) + std::string("_input")),
			  PowerRead(std::string("/sys/devices/3160000.i2c/i2c-0/0-00") + std::to_string(number_sensor) + std::string("/iio_device/in_current") + std::to_string(rail) + std::string("_input")),
			  PowerRead(std::string("/sys/devices/3160000.i2c/i2c-0/0-00") + std::to_string(number_sensor) + std::string("/iio_device/in_power") + std::to_string(rail) + std::string("_input")) }
{
	is_valid = true;
	for(int i = 0; i < 3; ++i) {
		is_valid &= drv[i].valid();
	}
}
std::array<double, 3> PowerReader::read()
{
	std::array<double, 3> a;
	for(int i = 0; i < 3; ++i) a[i] = drv[i].read_value();
	return a;
}
double PowerReader::read(int i)
{
	return drv[i].read_value();
}
bool PowerReader::valid() {
	return is_valid;
}
