/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/



#include "System.h"
#include "Converter.h"
#include <thread>
#include <pangolin/pangolin.h>
#include <iomanip>
#include <sys/types.h>
#include <sys/shm.h>

namespace ORB_SLAM2
{

System::System(const string &strVocFile, const string &strSettingsFile, const eSensor sensor,
               const bool bUseViewer):mSensor(sensor), mpViewer(static_cast<Viewer*>(NULL)), mbReset(false),mbActivateLocalizationMode(false),
        mbDeactivateLocalizationMode(false)
{
    // Output welcome message
    /*cout << endl <<
    "ORB-SLAM2 Copyright (C) 2014-2016 Raul Mur-Artal, University of Zaragoza." << endl <<
    "This program comes with ABSOLUTELY NO WARRANTY;" << endl  <<
    "This is free software, and you are welcome to redistribute it" << endl <<
    "under certain conditions. See LICENSE.txt." << endl << endl;*/

    cout << "Input sensor was set to: ";

    if(mSensor==MONOCULAR)
        cout << "Monocular" << endl;
    else if(mSensor==STEREO)
        cout << "Stereo" << endl;
    else if(mSensor==RGBD)
        cout << "RGB-D" << endl;

    //Check settings file
    cv::FileStorage fsSettings(strSettingsFile.c_str(), cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
       cerr << "Failed to open settings file at: " << strSettingsFile << endl;
       exit(-1);
    }


    //Load ORB Vocabulary
    cout << endl << "Loading ORB Vocabulary. This could take a while..." << endl;

    mpVocabulary = new ORBVocabulary();
    bool bVocLoad = false;
    if(strVocFile.substr( strVocFile.length() - 4) == ".bin")
        bVocLoad = mpVocabulary->loadFromBinaryFile(strVocFile);
    else
        bVocLoad = mpVocabulary->loadFromTextFile(strVocFile);
    
    if(!bVocLoad)
    {
        cerr << "Wrong path to vocabulary. " << endl;
        cerr << "Falied to open at: " << strVocFile << endl;
        exit(-1);
    }
    cout << "Vocabulary loaded!" << endl << endl;

    //Create KeyFrame Database
    mpKeyFrameDatabase = new KeyFrameDatabase(*mpVocabulary);

    //Create the Map
    mpMap = new Map();
    mapLoaded = false;
    //TODO: load the map!
    Tracking::eTrackingState tck = Tracking::eTrackingState::NO_IMAGES_YET;
    
    /* TODO: Federico mappa shared memory
    if(MapExists()){ //load map from disk space
		LoadMap("disk_map.yml");
		tck = Tracking::eTrackingState::LOST; //crasha quando va in OK?	
	}*/
    {
        cv::FileStorage mapFile("map.yml", cv::FileStorage::READ);
        if(mapFile.isOpened())
        {
            mpMap->loadMap(mapFile.root(), mpVocabulary, mpKeyFrameDatabase);
            mbActivateLocalizationMode = true;
            tck = Tracking::eTrackingState::LOST;
            mapLoaded = true;
            if(false)
            {
                cv::FileStorage mapFile("map.loaded.yml", cv::FileStorage::WRITE);
                mpMap->saveMap(mapFile);
                mpKeyFrameDatabase->write(mapFile);

                mapFile.release();
            }
        }
    }

    //Create Drawers. These are used by the Viewer
    mpFrameDrawer = new FrameDrawer(mpMap);
    mpMapDrawer = new MapDrawer(mpMap, strSettingsFile);

    //Initialize the Tracking thread
    //(it will live in the main thread of execution, the one that called this constructor)
    mpTracker = new Tracking(this, mpVocabulary, mpFrameDrawer, mpMapDrawer,
                             mpMap, mpKeyFrameDatabase, strSettingsFile, mSensor, tck);

    //Initialize the Local Mapping thread and launch
    mpLocalMapper = new LocalMapping(mpMap, mSensor==MONOCULAR);
    
    //Initialize the Loop Closing thread and launch
    mpLoopCloser = new LoopClosing(mpMap, mpKeyFrameDatabase, mpVocabulary, mSensor!=MONOCULAR);
    
    //Set pointers between threads
    mpTracker->SetLocalMapper(mpLocalMapper);
    mpTracker->SetLoopClosing(mpLoopCloser);

    mpLocalMapper->SetTracker(mpTracker);
    mpLocalMapper->SetLoopCloser(mpLoopCloser);

    mpLoopCloser->SetTracker(mpTracker);
    mpLoopCloser->SetLocalMapper(mpLocalMapper);
    
    mptLocalMapping = new thread(&ORB_SLAM2::LocalMapping::Run,mpLocalMapper);
    mptLoopClosing = new thread(&ORB_SLAM2::LoopClosing::Run, mpLoopCloser);

    //Initialize the Viewer thread and launch
    if(bUseViewer)
    {
        mpViewer = new Viewer(this, mpFrameDrawer,mpMapDrawer,mpTracker,strSettingsFile);
        mptViewer = new thread(&Viewer::Run, mpViewer);
        mpTracker->SetViewer(mpViewer);
    }
}

System::~System()
{
    //if(!mapLoaded)
    //{
    //    cv::FileStorage mapFile("map.yml", cv::FileStorage::WRITE);
    //    mpMap->saveMap(mapFile);
    //    mpKeyFrameDatabase->write(mapFile);

    //    mapFile.release();
    //}
    std::cout << "Deleting local resources" << std::endl << std::flush;
    std::cout << "Deleting Tracker... " << std::flush;
    delete mpTracker;
    std::cout << "OK!" << std::endl << std::flush;
    std::cout << "Deleting Local Mapper... " << std::flush;
    delete mpLocalMapper;
    std::cout << "OK!" << std::endl << std::flush;
    std::cout << "Deleting Loop Closer... " << std::flush;
    delete mpLoopCloser;
    std::cout << "OK!" << std::endl << std::flush;
    
    std::cout << "Deleting Frame Drawer... " << std::flush;
    delete mpFrameDrawer;
    std::cout << "OK!" << std::endl << std::flush;
    std::cout << "Deleting Map Drawer... " << std::flush;
    delete mpMapDrawer;
    std::cout << "OK!" << std::endl << std::flush;
    
    /*std::cout << "Deleting threads" << std::endl << std::flush;
    delete mptLocalMapping;
    delete mptLoopClosing;*/
    
    std::cout << "Deleting vocabulary... " << std::flush;
    delete mpVocabulary;
    std::cout << "OK!" << std::endl << std::flush;
    std::cout << "Deleting database... " << std::flush;
    delete mpKeyFrameDatabase;
    std::cout << "OK!" << std::endl << std::flush;
    std::cout << "Deleting map... " << std::flush;
    delete mpMap;
    std::cout << "OK!" << std::endl << std::flush;
    
    if(mpViewer != static_cast<Viewer *>(NULL))
    {
        std::cout << "Stopping viewer... " << std::flush;
        mpViewer->RequestFinish();
        if(mptViewer->joinable())
        {
            std::cout << "Join viewer... " << std::flush;
            mptViewer->join();
        }
        std::cout << "OK!" << std::endl << std::flush;
        std::cout << "Deleting thread viewer... " << std::flush;
        delete mptViewer;
        std::cout << "OK!" << std::endl << std::flush;
        std::cout << "Deleting viewer... " << std::flush;
        delete mpViewer;
        std::cout << "OK!" << std::endl << std::flush;
    }

    //Optimizer::saveTimes();
}

cv::Mat System::TrackStereo(const cv::Mat &imLeft, const cv::Mat &imRight, const double &timestamp, const double &current_time, int current_frame)
{
    if(mSensor!=STEREO)
    {
        cerr << "ERROR: you called TrackStereo but input sensor was not set to STEREO." << endl;
        exit(-1);
    }   

    // Check mode change
    {
        unique_lock<mutex> lock(mMutexMode);
        if(mbActivateLocalizationMode)
        {
            mpLocalMapper->RequestStop();

            // Wait until Local Mapping has effectively stopped
            while(!mpLocalMapper->isStopped())
            {
                usleep(1000);
            }

            mpTracker->InformOnlyTracking(true);
            mbActivateLocalizationMode = false;
        }
        if(mbDeactivateLocalizationMode)
        {
            mpTracker->InformOnlyTracking(false);
            mpLocalMapper->Release();
            mbDeactivateLocalizationMode = false;
        }
    }

    // Check reset
    {
    unique_lock<mutex> lock(mMutexReset);
    if(mbReset)
    {
        mpTracker->Reset();
        mbReset = false;
    }
    }

    cv::Mat Tcw = mpTracker->GrabImageStereo(imLeft,imRight,timestamp,current_time,current_frame);
    
    unique_lock<mutex> lock2(mMutexState);
    mTrackingState = mpTracker->mState;
    mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
    mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
    return Tcw;
}

cv::Mat System::TrackRGBD(const cv::Mat &im, const cv::Mat &depthmap, const double &timestamp, const double &current_time, int current_frame)
{
    if(mSensor!=RGBD)
    {
        cerr << "ERROR: you called TrackRGBD but input sensor was not set to RGBD." << endl;
        exit(-1);
    }    

    // Check mode change
    {
        unique_lock<mutex> lock(mMutexMode);
        if(mbActivateLocalizationMode)
        {
            mpLocalMapper->RequestStop();

            // Wait until Local Mapping has effectively stopped
            while(!mpLocalMapper->isStopped())
            {
                usleep(1000);
            }

            mpTracker->InformOnlyTracking(true);
            mbActivateLocalizationMode = false;
        }
        if(mbDeactivateLocalizationMode)
        {
            mpTracker->InformOnlyTracking(false);
            mpLocalMapper->Release();
            mbDeactivateLocalizationMode = false;
        }
    }

    // Check reset
    {
    unique_lock<mutex> lock(mMutexReset);
    if(mbReset)
    {
        mpTracker->Reset();
        mbReset = false;
    }
    }

    cv::Mat Tcw = mpTracker->GrabImageRGBD(im,depthmap,timestamp,current_time,current_frame);
    
    unique_lock<mutex> lock2(mMutexState);
    mTrackingState = mpTracker->mState;
    mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
    mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
    return Tcw;
}

cv::Mat System::TrackMonocular(const cv::Mat &im, const double &timestamp, const double &current_time, int current_frame)
{
    if(mSensor!=MONOCULAR)
    {
        cerr << "ERROR: you called TrackMonocular but input sensor was not set to Monocular." << endl;
        exit(-1);
    }

    // Check mode change
    {
        unique_lock<mutex> lock(mMutexMode);
        if(mbActivateLocalizationMode)
        {
            mpLocalMapper->RequestStop();

            // Wait until Local Mapping has effectively stopped
            while(!mpLocalMapper->isStopped())
            {
                usleep(1000);
            }

            mpTracker->InformOnlyTracking(true);
            mbActivateLocalizationMode = false;
        }
        if(mbDeactivateLocalizationMode)
        {
            mpTracker->InformOnlyTracking(false);
            mpLocalMapper->Release();
            mbDeactivateLocalizationMode = false;
        }
    }

    // Check reset
    {
    unique_lock<mutex> lock(mMutexReset);
    if(mbReset)
    {
        mpTracker->Reset();
        mbReset = false;
    }
    }
    cv::Mat Tcw = mpTracker->GrabImageMonocular(im,timestamp,current_time, current_frame);
    
    unique_lock<mutex> lock2(mMutexState);
    mTrackingState = mpTracker->mState;
    mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
    mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;

    return Tcw;
}

void System::ActivateLocalizationMode()
{
    unique_lock<mutex> lock(mMutexMode);
    mbActivateLocalizationMode = true;
}

void System::DeactivateLocalizationMode()
{
    unique_lock<mutex> lock(mMutexMode);
    mbDeactivateLocalizationMode = true;
}

bool System::MapChanged()
{
    static int n=0;
    int curn = mpMap->GetLastBigChangeIdx();
    if(n<curn)
    {
        n=curn;
        return true;
    }
    else
        return false;
}

void System::Reset()
{
    unique_lock<mutex> lock(mMutexReset);
    mbReset = true;
}

void System::Shutdown()
{
    std::cout << "Requesting finish local mapper... " << std::flush;
    mpLocalMapper->RequestFinish();
    std::cout << "OK!" << std::endl << std::flush;
    
    std::cout << "Requesting finish loop closing... " << std::flush;
    mpLoopCloser->RequestFinish();
    std::cout << "OK!" << std::endl << std::flush;
    if(mpViewer)
    {
        std::cout << "Requesting finish viewer... " << std::flush;
        mpViewer->RequestFinish();
        while(!mpViewer->isFinished())
        {
            std::cout << "not yet..." << std::flush;
            usleep(5000);
        }
        std::cout << "OK!" << std::endl << std::flush;
    }

    // Wait until all thread have effectively stopped
    while(!mpLocalMapper->isFinished() || !mpLoopCloser->isFinished() || mpLoopCloser->isRunningGBA())
    {
        std::cout << "Wait the global shut down..." << std::flush;
        usleep(5000);
    }
    std::cout << "OK!" << std::endl << std::flush;

    if(mpViewer)
    {
        std::cout << "Binding to the context... " << std::flush;
        //pangolin::BindToContext("ORB-SLAM2: Map Viewer");
        std::cout << "OK!" << std::endl << std::flush;
    }
    std::cout << "Everything shut down gracefully!" << std::endl << std::flush;
}

void System::SaveTrajectoryTUM(const string &filename, double fps_multiplier)
{
    cout << endl << "Saving camera trajectory to " << filename << " ..." << endl;
    if(mSensor==MONOCULAR)
    {
        cerr << "ERROR: SaveTrajectoryTUM cannot be used for monocular." << endl;
        return;
    }

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
    // We need to get first the keyframe pose and then concatenate the relative transformation.
    // Frames not localized (tracking failure) are not saved.

    // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
    // which is true when tracking failed (lbL).
    list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
    list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
    list<bool>::iterator lbL = mpTracker->mlbLost.begin();
    for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(),
        lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lbL++)
    {
        if(*lbL)
            continue;

        KeyFrame* pKF = *lRit;

        cv::Mat Trw = cv::Mat::zeros(4,4,CV_32F);
        seteye(Trw);

        // If the reference keyframe was culled, traverse the spanning tree to get a suitable keyframe.
        while(pKF->isBad())
        {
            Trw = Trw*pKF->mTcp;
            pKF = pKF->GetParent();
        }

        Trw = Trw*pKF->GetPose()*Two;

        cv::Mat Tcw = (*lit)*Trw;
        cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
        cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

        vector<float> q = Converter::toQuaternion(Rwc);

        f << setprecision(6) << *lT * fps_multiplier << " " <<  setprecision(9) << twc.at<float>(0) << " " << twc.at<float>(1) << " " << twc.at<float>(2) << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;
    }
    f.close();
    cout << endl << "trajectory saved!" << endl;
}


void System::SaveKeyFrameTrajectoryTUM(const string &filename, double fps_multiplier)
{
    cout << endl << "Saving keyframe trajectory to " << filename << " ..." << endl;

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    //cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    for(size_t i=0; i<vpKFs.size(); i++)
    {
        KeyFrame* pKF = vpKFs[i];

       // pKF->SetPose(pKF->GetPose()*Two);

        if(pKF->isBad())
            continue;

        cv::Mat R = pKF->GetRotation().t();
        vector<float> q = Converter::toQuaternion(R);
        cv::Mat t = pKF->GetCameraCenter();
        f << setprecision(6) << pKF->mTimeStamp * fps_multiplier << setprecision(7) << " " << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
          << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;

    }

    f.close();
    cout << endl << "trajectory saved!" << endl;
}

void System::SaveTrajectoryKITTI(const string &filename, double fps_multiplier)
{
    cout << endl << "Saving camera trajectory to " << filename << " ..." << endl;
    if(mSensor==MONOCULAR)
    {
        cerr << "ERROR: SaveTrajectoryKITTI cannot be used for monocular." << endl;
        //return;
    }

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
    // We need to get first the keyframe pose and then concatenate the relative transformation.
    // Frames not localized (tracking failure) are not saved.

    // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
    // which is true when tracking failed (lbL).
    list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
    list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
    list<int>::iterator lF = mpTracker->mlFrameNumber.begin();
    for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(), lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lF++)
    {
        ORB_SLAM2::KeyFrame* pKF = *lRit;

        cv::Mat Trw = cv::Mat::zeros(4,4,CV_32F);
        seteye(Trw);
        while(pKF->isBad())
        {
          //  cout << "bad parent" << endl;
            Trw = Trw*pKF->mTcp;
            pKF = pKF->GetParent();
        }

        Trw = Trw*pKF->GetPose()*Two;

        cv::Mat Tcw = (*lit)*Trw;
        cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
        cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

        f << setprecision(9) << *lF << " " << *lT << " " << Rwc.at<float>(0,0) << " " << Rwc.at<float>(0,1)  << " " << Rwc.at<float>(0,2) << " "  << twc.at<float>(0) << " " <<
             Rwc.at<float>(1,0) << " " << Rwc.at<float>(1,1)  << " " << Rwc.at<float>(1,2) << " "  << twc.at<float>(1) << " " <<
             Rwc.at<float>(2,0) << " " << Rwc.at<float>(2,1)  << " " << Rwc.at<float>(2,2) << " "  << twc.at<float>(2) << endl;
    }
    f.close();
    cout << endl << "trajectory saved!" << endl;
}

int System::GetTrackingState()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackingState;
}

vector<MapPoint*> System::GetTrackedMapPoints()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackedMapPoints;
}
vector<MapPoint*> System::GetAllMapPoints()
{
    unique_lock<mutex> lock(mMutexState);
    return mpTracker->GetAllMapPoints();
}
vector<cv::KeyPoint> System::GetTrackedKeyPointsUn()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackedKeyPointsUn;
}


//Loads the map from disk space, must be called only if MapExists returns true

void System::LoadMap(const string &filename){
	
	if(!MapExists()){
		cout << endl << "Map does not exist!" << endl;
	}
	
	 cout << endl << "Reading map from " << filename << " ..." << endl;
	
	
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	cv::FileStorage mapFile(filename, cv::FileStorage::READ);
    mpMap->loadMap(mapFile.root(), mpVocabulary, mpKeyFrameDatabase); 

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		
	
	cout << endl << "Map loaded from " << filename << " in " <<  std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms"<< endl;
	
}


//Saves the current map to disk space and sets the shared boolean flag to true

void System::SaveMap(const string &filename){
	
	 cout << endl << "Saving map to " << filename << " ..." << endl;
	
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	 cv::FileStorage mapFile(filename, cv::FileStorage::WRITE); //salva mappa
     mpMap->saveMap(mapFile);
     mpKeyFrameDatabase->write(mapFile);

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		
	int shmid =  shmget(SHMKEY,sizeof(bool), 0644 ); //check if shared memory block has already been created
	
	if(shmid == -1){ //Shared memory block doesn't exists
		std::cout << "Error while writing boolean flag in shared memory in System.cc SaveMap,shared memory block does not exists!" << std::endl << std::flush;
			  exit(-1);
		}
		bool* mapFlag = (bool*)shmat(shmid, (void *)0, 0);
		*mapFlag = true; //Set boolean flag to true since the map has been saved
	
	cout << endl << "Map saved in "<< std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms " << "number of keyframes = " << mpMap->KeyFramesInMap() 
			<< " number of map points = "<< mpMap->MapPointsInMap() << endl;
}



//Checks if exists a map saved by a previous version
bool System::MapExists(){
	int shmid =  shmget(SHMKEY,sizeof(bool), 0644 ); //check if shared memory block has already been created
	
	if(shmid == -1){ //Shared memory block doesn't exists
		std::cout << "Memory does not exists!" << std::endl << std::flush;
		return false;
	}

	bool* mapFlag = (bool*)shmat(shmid, (void *)0, 0);
	return *mapFlag; //if a previous map exists it will return true
	
}

void System::savemVelocity(){
	mpTracker->savemVelocity();
}




/*void System::deployORBVersion(int choosedVersion){

		switch(choosedVersion){
	
			case powersaving:
				//TODO deploy power saving version: (for example turn off cuda, openmp, openvx load model 30)
				break;
		
			case normal:
				//TODO deploy normal version: (for example turn off cuda, openmp, openvx , set frequencies to dynamic, load model 10)
				break;

			case performance:
				//TODO deploy performance version: (for example turn on openmp, load model 13 )
				break;
	
			deafult:
				std::cout << "Error while parsing choosed version" << std::endl;
				break;
					
	}
	
}*/
} //namespace ORB_SLAM
