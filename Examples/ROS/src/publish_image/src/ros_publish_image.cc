// Parameters

// send_topic: the topic to publish images (default /camera/image_still)
// image_file: the path to the image file to be published


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>

using namespace std;

void shutdown(const std_msgs::String::ConstPtr& msg)
{
    ros::shutdown();
}

int main(int argc, char **argv)
{
    //if(argc != 2)
    //{
    //    cerr << endl << "Usage: ./ros_mono_kitti_source path_to_sequence" << endl;
    //    return 1;
    //}

    ros::init(argc, argv, "publish_image");
    ros::start();
    
    ros::NodeHandle nodeHandler("~");
    
    std::string topic;
    std::string image;
    nodeHandler.param<std::string>("send_topic", topic, "/camera/image_raw");
    nodeHandler.param<std::string>("image_file", image, "/home/nvidia/Desktop/11/image_0/000012.png");

    image_transport::ImageTransport it(nodeHandler);
    image_transport::Publisher pub = it.advertise(topic.c_str(), 1, true);
    ros::Subscriber shutdown_sub = nodeHandler.subscribe(std::string("/shutdown"), 1, shutdown);
    
    ros::Rate r(1);
    
    // Main loop
    cv::Mat im;
    im = cv::imread(image,CV_LOAD_IMAGE_UNCHANGED);
    if(! im.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    while(ros::ok())
    {
        
        sensor_msgs::ImageConstPtr msg;
        if(im.channels() == 1)
            msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", im).toImageMsg();
        else
            msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", im).toImageMsg();
        pub.publish(msg);
        
        r.sleep();
    }
    
    ros::shutdown();

    return 0;
}


