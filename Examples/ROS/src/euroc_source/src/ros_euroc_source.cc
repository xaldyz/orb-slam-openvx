/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

// Parameters

// send_topic: the topic to publish images (default /camera/image_raw)
// sequence_path: the path of kitti sequence


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>

#include "data.hpp"

using namespace std;

void simulation_step(double time_processed, vector<double> vTimestamps, int start_idx, simulation_time_status& s)
{
	s.frame_offset = 0;
	s.wait_time    = 0;
	
	int n_images = vTimestamps.size();
	int next_idx = start_idx + s.frames_to_advance;
	
	double start_time = s.time;//vTimestamps[start_idx];
	double final_time_processed = start_time + time_processed;
	
	if(next_idx >= n_images)
	{
		// End of the stream: special handling
		s.frame_offset = 0;
		s.wait_time = (s.skip) ? std::max(0.0, vTimestamps[n_images-1] - final_time_processed) : 0.0;//wait the complete acquisition
		s.time += time_processed + s.wait_time;
		return;
	}
	
	if(!s.skip)
	{
		//no skip frame, only wait if processing time was short enough
		s.wait_time = std::max(0.0, vTimestamps[next_idx] - ( vTimestamps[start_idx] + time_processed )); 
		//std::cout << "Wait time: " << s.wait_time << " " << vTimestamps[next_idx] << " " << vTimestamps[start_idx] << " " << time_processed << std::endl;
		s.time += time_processed + s.wait_time;
		return;
	}
	
	
	double next_time = vTimestamps[next_idx];
	if(final_time_processed <= next_time)
	{
		//case 1
		//processing did well, need to wait (THE ONLY CASE!) and no more delay!
		s.wait_time = next_time - final_time_processed;
		s.time += time_processed + s.wait_time;
	}
	else
	{
	    s.time += time_processed;
		//offset because do/while
		s.frame_offset = 0;
        double prev_time = vTimestamps[start_idx];
        double cur_time  = next_time;
        double delta_time = cur_time - prev_time;
		do
		{
			s.frame_offset += s.frames_to_advance;
			
			if(start_idx+1+s.frame_offset < n_images)
			    cur_time = vTimestamps[start_idx+1+s.frame_offset];
			else
			    cur_time = prev_time + delta_time;
			delta_time = cur_time - prev_time;
			prev_time  = cur_time;
		}while(cur_time < s.time);
		//recover a step if the time is not a perfect multiple
		//(if a perfect multiple, the execution finished when new frame is ready)
		if(cur_time != s.time) s.frame_offset -= s.frames_to_advance;
	}
}



void LoadImages(const string &strPathToSequence, vector<string> &vstrImageFilenames,
                vector<double> &vTimestamps, double fps_multiplier, bool right) {
    ifstream fTimes;
    string strPrefixLeft = strPathToSequence + ((right) ? "/cam1/" : "/cam0/");
    string strPathTimeFile = strPrefixLeft + "/data.csv";
    fTimes.open(strPathTimeFile.c_str());
    string s;
    getline(fTimes,s);
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            long tt;
            ss >> tt;
            double t;
            t = 1.0 * tt / 1e9;
            vTimestamps.push_back(t / fps_multiplier);
            string s1;
            char d;
            ss >> d;
            ss >> s1;
            
            vstrImageFilenames.push_back(strPrefixLeft + "data/" + s1);
        }
    }
/*
    const int nTimes = vTimestamps.size();
    vstrImageFilenames.resize(nTimes);

    for(int i=0; i<nTimes; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        vstrImageFilenames[i] = strPrefixLeft + ss.str() + ".png";
    }*/
}


int main(int argc, char **argv)
{
    //if(argc != 2)
    //{
    //    cerr << endl << "Usage: ./ros_mono_kitti_source path_to_sequence" << endl;
    //    return 1;
    //}

    ros::init(argc, argv, "euroc_source");
    ros::start();
    
    ros::NodeHandle nodeHandler("~");
    
    simulation_time_status s;
    s.frames_to_advance = 1;
    s.time = 0;
    s.realtime = true;
	s.skip     = false;

    std::string topic;
    std::string sequence_path;
    double fps_multiplier;
    nodeHandler.param<double>("fps_multiplier", fps_multiplier, 1);
    nodeHandler.param<std::string>("send_topic", topic, "/camera/image_raw");
    if (!nodeHandler.getParam("sequence_path", sequence_path)) {
        ROS_ERROR("Missing sequence_path! Aborting...");
    }
    int number_subscriber = 2;
    nodeHandler.param<int>("sub_to_wait", number_subscriber, 2);
    bool shutdown = true;
    nodeHandler.param<bool>("shutdown", shutdown, true);
    bool right_image = false;
    nodeHandler.param<bool>("right_image", right_image, false);
    
    std::string setting_file = "";
    nodeHandler.param<string>("setting_file", setting_file, "");
    

    image_transport::ImageTransport it(nodeHandler);
    image_transport::Publisher pub = it.advertise(topic.c_str(), 1, true);

    //Powermon p;
    //p.prepare();
    
        // Read rectification parameters
    // -------------------------------------------------
    cv::FileStorage fsSettings(setting_file, cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
        cerr << "ERROR: Wrong path to settings" << endl;
        return -1;
    }
    cv::FileNode filenode = fsSettings["LEFT.K"];

    cv::Mat K, P, R, D;
    cv::Mat M1,M2;
    if(!filenode.isNone())
    {
        int rows = 0;
        int cols = 0;
        if(right_image) {
            fsSettings["RIGHT.K"] >> K;
            fsSettings["RIGHT.P"] >> P;
            fsSettings["RIGHT.R"] >> R;
            fsSettings["RIGHT.D"] >> D;
            rows = fsSettings["RIGHT.height"];
            cols = fsSettings["RIGHT.width"];
        } else {
            fsSettings["LEFT.K"] >> K;
            fsSettings["LEFT.P"] >> P;
            fsSettings["LEFT.R"] >> R;
            fsSettings["LEFT.D"] >> D;
            rows = fsSettings["LEFT.height"];
            cols = fsSettings["LEFT.width"];
        }

        if(K.empty() || P.empty() || R.empty() || D.empty() || rows==0 || cols==0 )
        {
            cerr << "ERROR: Calibration parameters to rectify stereo are missing!" << endl;
            return -1;
        }

        cv::initUndistortRectifyMap(K,D,R,P.rowRange(0,3).colRange(0,3),cv::Size(cols,rows),CV_32F,M1,M2);   
    }
    // ------------------ END -----------------------

    // Retrieve paths to images
    vector<string> vstrImageFilenames;
    vector<double> vTimestamps;
    std::cout << "Loading images!" << std::endl << std::flush;
    LoadImages(sequence_path, vstrImageFilenames, vTimestamps, fps_multiplier, right_image);
    std::cout << "End images load!" << std::endl << std::flush;
    s.time = vTimestamps[0];

    int nImages = vstrImageFilenames.size();
    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;
    cout << "Images in the sequence: " << nImages << endl << endl;

    // Main loop
    cv::Mat im;
    cv::Mat imRect;
    {
        ros::Rate r(10);
        while(ros::ok())
        {
            int nsub = pub.getNumSubscribers();
            std::cout << nsub << std::endl;//wait until two subscription!
            if(nsub >= number_subscriber)
            {
                break;
            }
            
            r.sleep();
        }
    }
    
    std::cout << "Starting the publisher " << std::endl;
    //usleep(10000000);//just wait, maybe this can solve the hanging problem

    //p.resetDataCollected();
    //p.startAsync();
    //nImages = 100;
    //nImages = 400;
    for(int nLoop = 0; nLoop < 1; nLoop++)
        for(int ni=0; ni<nImages && ros::ok(); ni++)
        {
#ifdef COMPILEDWITHC11
            std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
#else
            std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
#endif
            // Read image from file
            im = cv::imread(vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
            cv::cvtColor(im, im, CV_GRAY2BGR);
            if(!filenode.isNone())
                cv::remap(im,imRect,M1,M2,cv::INTER_LINEAR);
            else
                imRect = im;
            double tframe = vTimestamps[ni];

            if(im.empty())
            {
                cerr << endl << "Failed to load image at: " << vstrImageFilenames[ni] << endl;
                return 1;
            }

            // Pass the image to the SLAM system via ROS
            // TODO tframe is not used...
            //std::cout << "Publishing image " << ni << std::endl;
            cv_bridge::CvImage img_cv(std_msgs::Header(), "bgr8", imRect);
            img_cv.header.seq = ni;
            img_cv.header.stamp = ros::Time(tframe);
            sensor_msgs::ImageConstPtr msg = img_cv.toImageMsg();
            pub.publish(msg);
            ros::spinOnce();

#ifdef COMPILEDWITHC11
            std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
#else
            std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
#endif
            double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();


            // Wait to load the next frame
            /*double T=0;
            if(ni<nImages-1)
                T = vTimestamps[ni+1]-tframe;
            else if(ni>0)
                T = tframe-vTimestamps[ni-1];

            if(ttrack<T)
                usleep((T-ttrack)*1e6);*/
                // Wait to load the next frame
            if(s.realtime)
            {
		        simulation_step(ttrack, vTimestamps, ni, s);
		        ni += s.frame_offset;
		        if(s.frame_offset > 0)
		        {
		            //std::cout << "Skipping " << s.frame_offset << " frames" << std::endl;
		        }
		        if(s.wait_time > 0)
                	usleep(s.wait_time*1e6);
	        }
        }
        
    if(!shutdown)
    {
        std::cout << "Shutdown command" << std::endl;
        ros::Rate r(1);
        while(ros::ok())
        {
            r.sleep();
        }
    }
    
    std::cout << "Shutting down command" << std::endl << std::flush;
    ros::Publisher pub_shutdown = nodeHandler.advertise<std_msgs::String>("/shutdown", 1, true);
    pub_shutdown.publish(std::string("1"));
    {
        ros::Rate r(10);
        while(ros::ok())
        {
            int nsub = pub.getNumSubscribers();
            std::cout << nsub << std::endl;//wait until two subscription!
            if(nsub == 0)
            {
                break;
            }
            
            r.sleep();
        }
    }
    
    //usleep(3000000);//just wait, maybe this can solve the hanging problem
    

    //p.stopAsync();

    //p.readSync();
    //p.printStats();

    ros::shutdown();

    return 0;
}


