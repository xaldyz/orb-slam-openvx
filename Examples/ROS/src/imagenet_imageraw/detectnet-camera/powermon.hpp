/*------------------------------------------------------------------------------
Copyright © 2016 by Nicola Bombieri

XLib is provided under the terms of The MIT License (MIT):

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/
/**
 * @author Federico Busato, Michele Scala
 * Univerity of Verona, Dept. of Computer Science
 * federico.busato@univr.it
 */
#pragma once

#include <fstream>
#include <thread>
#include <mutex>

#include <stdint.h>
#include <sys/termios.h>
#include <time.h>
#include <stdio.h>

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>

#include<sstream>
namespace std{
template <typename T>
std::string to_string(T value);
template<>
std::string to_string<int>(int v);
}

typedef struct measure {
    double voltage;
    double current;
    double power;
} measure;

class Powermon {

public:
    virtual bool start();
    virtual bool start(std::chrono::time_point<std::chrono::steady_clock> t );
    virtual bool stop();

    virtual bool reset();

    virtual void log();

    double getPowerMax();
    double getPowerAvg();
    double getRawPowerTotal();
    double getPowerTotal();
    int getSampledInstants();
    void printStats();

protected:
    Powermon();
    ~Powermon();
    Powermon(std::ofstream* _log);

    std::chrono::steady_clock::time_point start_time;
    std::chrono::steady_clock::time_point stop_time;

    measure last_power_read[12];
    double power_max;
    double power_total;
    int    sampled_instants;

    bool   running;
  
    const bool           log_enabled;
    std::ofstream*        log_stream;
};

class PowerRead
{
	int fd;
	bool is_valid;
public:
	PowerRead(const char *c);
	PowerRead(std::string s);
	
	double read_value();
	bool valid();
};

class PowermonExternal : public Powermon {

static const int VERSION_H = 2;
static const int VERSION_L = 2;
static const int OVF_FLAG = 7;
static const int TIME_FLAG = 6;
static const int DONE_FLAG = 5;

#define BAUDRATE B1000000
static constexpr double V_FULLSCALE = 26.52;
static constexpr double R_SENSE     = 0.00422;    // found empirically
static constexpr double I_FULLSCALE = 0.10584;

public:

    /**
     * @brief Create the Powermoon object.
     * @details
     *
     * @param p Name of the device port ( e.g. /dev/ttyUSB0 ).
     * @param m Bit Mask of sensors, every 1 bit enable a sensor.
    */
    PowermonExternal();
    ~PowermonExternal();
    PowermonExternal(std::ofstream* _log);
    PowermonExternal(const char* _device, uint16_t _mask);
    PowermonExternal(const char* _device, uint16_t _mask, std::ofstream* _log);
    PowermonExternal(const char* _device, uint16_t _mask, std::ofstream* _log, int _sampling_interval);

    /**
     * @brief Start getting samples from the powermoon.
     * @details Start getting samples from the powermoon, it's a blocking function, it waits that powermoon starts sending samples before return.
     *
     * @return 1 if something got wrong
     */
    bool start();
    bool start(std::chrono::time_point<std::chrono::steady_clock> t );
    /**
     * @brief Stops getting samples if it is running.
     * @details Stops getting samples if it is running.
     */
    bool stop();
    
    void log();
    /**
     * @brief Reset the data.
     * @details Reset the data.
     */
    //bool reset();

private:
    //const int N_OF_SAMPLING = 30000;
	//8160 massimi sample che danno valore corretto!
	const int N_OF_SAMPLING = 8160;
    //const int N_OF_SAMPLING = 0;

    //PARAMS
    const char*          device;
    const unsigned short mask;
    
    int sampling_interval;
    int active_sensors;

    bool powermon_found = false;
    /**
     * @brief Configure the tty, connect and open it
     * @return 0 all ok, !=0 if there is some errors
     */
    bool configure();
    
    void prepare();

    void Reset(bool err = true);
    /**
     * @brief Set sensors mask to powermoon
     * @return 0 all ok, !=0 if there is some errors
     */
    void setMask();
    /**
     * @brief Set the number of samples to powermoon
     * @return 0 all ok, !=0 if there is some errors
     */
    void setSamples();
    /**
     * @brief Set the current time to powermoon
     * @return 0 all ok, !=0 if there is some errors
     */
    void setTime();

    void getVersion();

    //THREAD
    std::mutex    mutex;
    std::thread*  supportThread;
    static void   task(PowermonExternal* ptr);
    bool          stop_task;
    volatile bool first_read;
    volatile bool should_read;
    volatile bool should_reset;

    //SERIAL
    int    pw_file_descriptor;
    FILE*  pw_file_pointer;
    
    char* dummy_char;

    //CONFIG
    struct termios pw_config, pw_old_config;
    
    measure measure_list[2000];
    int current_measure;
    std::chrono::steady_clock::time_point tmp_time;
};


class PowerReader
{
	bool is_valid;
	PowerRead drv[3];
public:
	PowerReader(int number_sensor, int rail);
	std::array<double, 3> read();
	double read(int i);
	bool valid();
};

class PowermonInternal : public Powermon {

public:
    PowermonInternal();
    ~PowermonInternal();
    PowermonInternal(std::ofstream* _log);
    
    /**
     * @brief Start getting samples from the powermoon.
     * @details Start getting samples from the powermoon, it's a blocking function, it waits that powermoon starts sending samples before return.
     *
     * @return 1 if something got wrong
     */
    bool start();
    bool start(std::chrono::time_point<std::chrono::steady_clock> t );
    /**
     * @brief Stops getting samples if it is running.
     * @details Stops getting samples if it is running.
     */
    bool stop();
    
    /**
     * @brief Reset the data.
     * @details Reset the data.
     */
    //bool reset();

private:
    bool powermon_found = false;

    //THREAD
    std::mutex    mutex;
    std::thread*  supportThread;
    static void   task(PowermonInternal* ptr);
    bool          stop_task;

    //SERIAL
    PowerReader *power_reader[12];
    /*PowerReader power_reader_40_0;
    PowerReader power_reader_40_1;
    PowerReader power_reader_40_2;
    PowerReader power_reader_41_0;
    PowerReader power_reader_41_1;
    PowerReader power_reader_41_2;
    PowerReader power_reader_42_0;
    PowerReader power_reader_42_1;
    PowerReader power_reader_42_2;
    PowerReader power_reader_43_0;
    PowerReader power_reader_43_1;
    PowerReader power_reader_43_2;*/
    
    char* dummy_char;

    //CONFIG
    struct termios pw_config, pw_old_config;
};
