/*------------------------------------------------------------------------------
Copyright © 2016 by Nicola Bombieri

XLib is provided under the terms of The MIT License (MIT):

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/
/**
 * @author Federico Busato, Michele Scala
 * Univerity of Verona, Dept. of Computer Science
 * federico.busato@univr.it
 */
#include <exception>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <limits>
#include <numeric>
#include <cmath>
#include <fstream>

#include "powermon.hpp"

Powermon::Powermon() : log_enabled(false), log_stream(NULL) {
	reset();
}

Powermon::Powermon(std::ofstream* _log) : log_enabled(true), log_stream(_log) {
	reset();
}

Powermon::~Powermon() {
    log_stream->flush();
    log_stream->close();
	
}

//------------------------------------------------------------------------------

bool Powermon::reset()
{
    if(running) return false;
    
    for(int i = 0; i < 12; ++i) {
        last_power_read[i].voltage = 0;
        last_power_read[i].current = 0;
        last_power_read[i].power   = 0;
    }
	this->power_max        = 0;
	this->power_total      = 0;
	this->sampled_instants = 0;
	
	this->running = false;
	
	return true;
}

//------------------------------------------------------------------------------

int Powermon::getSampledInstants() {
    return this->sampled_instants;
}

double Powermon::getPowerMax() {
    return this->power_max;
}

double Powermon::getPowerAvg() {
    return (double) this->power_total / this->sampled_instants;
}

double Powermon::getRawPowerTotal() {
    return this->power_total;
}

double Powermon::getPowerTotal() {
    if(running) {
		stop_time = std::chrono::steady_clock::now();
	}
    return this->power_total / this->sampled_instants * std::chrono::duration_cast<std::chrono::seconds>(stop_time - start_time).count();
}

bool Powermon::start() {
	return start(std::chrono::steady_clock::now());
}
bool Powermon::start( std::chrono::time_point<std::chrono::steady_clock> t ) {
    running = true;
    start_time = t;
	return true;
}

bool Powermon::stop() {
	running = false;
	stop_time = std::chrono::steady_clock::now();
	return true;
}

void Powermon::log() {
    if(log_enabled)
    {
        stop_time = std::chrono::steady_clock::now();
        (*log_stream) << std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count();
        for(int i = 0; i < 12; ++i) (*log_stream) << ";" << last_power_read[i].voltage
                                                  << ";" << last_power_read[i].current
                                                  << ";" << last_power_read[i].power;
        (*log_stream) << std::endl;
    }
}

//------------------------------------------------------------------------------

void Powermon::printStats()
{
	if(running) {
		stop_time = std::chrono::steady_clock::now();
	}
	
    std::cout << "Power avg         : " << getPowerAvg()        << " W" << std::endl;
    std::cout << "Power max         : " << getPowerMax()        << " W" << std::endl;
    std::cout << "Power total       : " << getPowerTotal()      << " J" << std::endl;
    std::cout << "Sampled instatnts : " << getSampledInstants() << " instants" << std::endl;
    std::cout << "Time              : " << std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count() << " ms" << std::endl;
}


namespace std{
	template <typename T>
	std::string to_string(T value)
	{
		//create an output string stream
		std::ostringstream os ;

		//throw the value into the string stream
		os << value ;

		//convert the string stream into a string and return
		return os.str() ;
	}
}
