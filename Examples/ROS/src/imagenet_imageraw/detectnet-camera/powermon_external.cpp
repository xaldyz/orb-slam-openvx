/*------------------------------------------------------------------------------
Copyright © 2016 by Nicola Bombieri

XLib is provided under the terms of The MIT License (MIT):

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/
/**
 * @author Federico Busato, Michele Scala
 * Univerity of Verona, Dept. of Computer Science
 * federico.busato@univr.it
 */
#include <exception>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <limits>
#include <numeric>
#include <cmath>
#include <fstream>

#include "powermon.hpp"

PowermonExternal::PowermonExternal() : Powermon(NULL), device("/dev/ttyUSB0"),  mask(128u),
sampling_interval(1) {
    active_sensors = __builtin_popcount (mask);
}

PowermonExternal::PowermonExternal(std::ofstream* _log) : Powermon(_log),
                        device("/dev/ttyUSB0"),  mask(128u),
sampling_interval(1) {
    active_sensors = __builtin_popcount (mask);
}

PowermonExternal::PowermonExternal(const char* _device, uint16_t _mask) : Powermon(NULL),
                        device(_device),  mask(_mask), 
sampling_interval(1) {
    active_sensors = __builtin_popcount (mask);
}

PowermonExternal::PowermonExternal(const char* _device, uint16_t _mask, std::ofstream* _log) : Powermon(_log),
                        device(_device),  mask(_mask), sampling_interval(1) {
                            active_sensors = __builtin_popcount (mask);
                        }

PowermonExternal::PowermonExternal(const char* _device, uint16_t _mask, std::ofstream* _log, int _si) : Powermon(_log), 
                        device(_device),  mask(_mask), sampling_interval(_si) {
                            active_sensors = __builtin_popcount (mask);
                        }

PowermonExternal::~PowermonExternal() {}

//------------------------------------------------------------------------------

bool PowermonExternal::configure() {
    this->pw_file_descriptor = ::open(device, O_RDWR | O_NOCTTY);
    if (this->pw_file_descriptor < 0)
        //throw std::runtime_error("PowermonExternal::Configure : TTY not found");
        return 0;
    if ((this->pw_file_pointer = ::fdopen(pw_file_descriptor, "r+")) == NULL)
        throw std::runtime_error("PowermonExternal::Configure : Can't open TTY");

    ::tcgetattr(this->pw_file_descriptor, &pw_old_config); /* save current port settings */
    ::bzero(&pw_config, sizeof (pw_config));

    /*
    CRTSCTS == hardware flow control
    CS8 = 8 bits
    CLOCAL = ignore modem control lines
    CREAD = enable receiver
    B38400 = baud rate
    */
    /* configure port */
    this->pw_config.c_iflag = IGNPAR;
    this->pw_config.c_cflag = BAUDRATE | CS8 | CSTOPB | CLOCAL | CREAD;
    this->pw_config.c_oflag = 0;

    /* set input mode (non-canonical, no echo,...) */
    this->pw_config.c_lflag = 0;

    this->pw_config.c_cc[VTIME] = 0; /* inter-character timer unused */
    this->pw_config.c_cc[VMIN] = 1; /* blocking read until 1 char received */

    ::tcflush(this->pw_file_descriptor, TCIFLUSH);
    ::tcsetattr(this->pw_file_descriptor, TCSANOW, &pw_config);
    return 1;
}

void PowermonExternal::prepare()
{
	if (this->configure()) {
        this->powermon_found = true;
        this->power_max = 0;
        this->power_total = 0;
        this->sampled_instants = 0;
    
		this->Reset();
        this->setMask();
        this->setSamples();
        this->setTime();
    } else {
        this->power_max = NAN;
        this->power_total = NAN;
        this->sampled_instants = static_cast<int>(NAN);
        std::cerr << "Not able to connect to Powermon" << std::endl;
    }
}
	
void PowermonExternal::Reset(bool err) {
    char buffer[32];

    ::fprintf(this->pw_file_pointer, "d\n");
    ::usleep(100000);
    ::fflush(this->pw_file_pointer);
    ::usleep(100000);
    ::tcflush(this->pw_file_descriptor, TCIFLUSH);
    ::fprintf(this->pw_file_pointer, "\n");
    //get result
    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);
    if (err && ::strcmp(buffer, "OK\r\n") != 0) {
        std::cerr << "-> " << buffer << std::endl;
        throw std::runtime_error("PowermonExternal::Reset");
    }
}

void PowermonExternal::setMask() {
    char buffer[32];

    unsigned length = ::sprintf(buffer, "m %u\n", this->mask);
    ::fwrite(buffer, 1, length, this->pw_file_pointer);
    //get response
    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);

    unsigned setval;
    ::sscanf(buffer, "M=%u\r", &setval);
    //get result
    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);
    if (::strcmp(buffer, "OK\r\n") != 0)
        throw std::runtime_error("PowermonExternal::setMask");
}

void PowermonExternal::setSamples() {
    char buffer[32];

    //unsigned length = ::sprintf(buffer, "s %u %u\n", sampling_interval, N_OF_SAMPLING);
    unsigned length = ::sprintf(buffer, "s %u %u\n", sampling_interval, 0);
    ::fwrite(buffer, 1, length, this->pw_file_pointer);
    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);

    unsigned set_interval, set_num_samples;
    ::sscanf(buffer, "S=%u,%u\r", &set_interval, &set_num_samples);

    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);
    if (::strcmp(buffer, "OK\r\n") != 0)
        throw std::runtime_error("PowermonExternal::setSamples");
}

void PowermonExternal::setTime() {
    char buffer[32];

    unsigned length = ::sprintf(buffer, "t %u\n", 1);
    ::fwrite(buffer, 1, length, this->pw_file_pointer);
    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);

    unsigned setval;
    ::sscanf(buffer, "T=%u\r", &setval);

    dummy_char = ::fgets(buffer, sizeof(buffer), this->pw_file_pointer);
    if (::strcmp(buffer, "OK\r\n") != 0)
        throw std::runtime_error("PowermonExternal::setTime");
}

//------------------------------------------------------------------------------

void PowermonExternal::getVersion() {
    char buffer[128];
    int length;

    length = sprintf(buffer, "v\n");
    fwrite(buffer, 1, length, pw_file_pointer);

    //get response
    dummy_char = fgets(buffer, sizeof(buffer), pw_file_pointer);
    std::cout << "Version: " << buffer << std::endl;

    dummy_char = fgets(buffer, sizeof(buffer), pw_file_pointer);
    std::cout << buffer << std::endl;

    dummy_char = fgets(buffer, sizeof(buffer), pw_file_pointer);
    std::cout << buffer << std::endl;

    //get result
    dummy_char = fgets(buffer, sizeof(buffer), pw_file_pointer);
    if (::strcmp(buffer, "OK\r\n") != 0)
        throw std::runtime_error("PowermonExternal::getVersion");
}

//------------------------------------------------------------------------------

bool PowermonExternal::start() {
    return start(std::chrono::steady_clock::now());
}

bool PowermonExternal::start(std::chrono::time_point<std::chrono::steady_clock> t ) {
    if (this->configure()) {
        this->powermon_found = true;
        this->power_max = 0;
        this->power_total = 0;
        this->sampled_instants = 0;

        this->stop_task = false;
        this->first_read = false;
        this->should_read = false;
        this->Reset();
        this->setMask();
        this->setSamples();
        this->setTime();
        for(int i = 0; i < 12; i++)
        {
            last_power_read[i].voltage = 0;
            last_power_read[i].current = 0;
            last_power_read[i].power   = 0;
        }
        current_measure = 0;

        supportThread = new std::thread(PowermonExternal::task, this);
        while(!first_read);
        tmp_time = t;
        return true && Powermon::start(t);
    } else {
        this->power_max = NAN;
        this->power_total = NAN;
        this->sampled_instants = static_cast<int>(NAN);
        std::cerr << "Not able to connect to Powermon" << std::endl;
        return false;
    }
}

bool PowermonExternal::stop() {
    if (!powermon_found)
        return true;
    stop_task = true;
    (*supportThread).join();
    delete supportThread;

    ::tcflush(this->pw_file_descriptor, TCIFLUSH);
    ::tcsetattr(this->pw_file_descriptor, TCSANOW, &pw_old_config);
    ::close(this->pw_file_descriptor);

    return true && Powermon::stop();
}

void PowermonExternal::log() {
    if(log_enabled)
    {
        stop_time = std::chrono::steady_clock::now();
        long long tt = std::chrono::duration_cast<std::chrono::milliseconds>(tmp_time - start_time).count();
        long long total_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - tmp_time).count();
        tmp_time = stop_time;
        for(int tm = 0; tm < current_measure; ++tm)
        {
            (*log_stream) << tt+1.0*total_time/current_measure*tm;
            for(int i = 0; i < 12; ++i) (*log_stream) << ";" << last_power_read[i].voltage
                                                      << ";" << last_power_read[i].current
                                                      << ";" << last_power_read[i].power;
            (*log_stream) << std::endl;
        }
        current_measure = 0;
    }
}

//------------------------------------------------------------------------------

void PowermonExternal::task(PowermonExternal* ptr){
    size_t dummy_size;
    PowermonExternal* master = (PowermonExternal*) ptr;

    ::fprintf(master->pw_file_pointer, "e\n");

    int times[8] = {1, 1, 1, 1, 1, 1, 1, 1};
    std::stringstream ss_mod3;
    int sensor_index = 0;
    double power_sensor[master->active_sensors];

    while (true) {
        unsigned char buffer[4];
        dummy_size = ::fread(buffer, 1, 4, master->pw_file_pointer);
        uint8_t flags = buffer[0];

        if (!(flags & (1<<TIME_FLAG)) && (flags & (1<<DONE_FLAG))) break;
        if(flags & (1 << OVF_FLAG))
            std::cerr << "*" << std::endl;
        if (flags & (1 << TIME_FLAG)) {
            unsigned int time = (((uint32_t)buffer[0] << 24) | ((uint32_t)buffer[1] << 16)
                                | ((uint32_t)buffer[2] << 8) | ((uint32_t)buffer[3]))
                                & 0x3FFFFFFF;
            master->log();
            master->current_measure = 0;
            //std::cerr << "timestamp: " << time <<std::endl;
        } else {
            master->first_read = true;
            
            //std::cout << master->should_reset << std::endl << std::flush;
            /*if(master->should_reset)
            {
                master->power_max = 0;
                master->power_total = 0;
                master->sampled_instants = 0;
                master->should_reset = false;
            }*/
            //if(!master->should_read) continue;

            uint8_t sensor = (uint8_t) buffer[0] & 0x0F;
            uint16_t voltage = ((uint16_t) buffer[1] << 4) | ((buffer[3] >> 4) & 0x0F);
            uint16_t current = ((uint16_t) buffer[2] << 4) | (buffer[3] & 0x0F);
            double v_double = (double) voltage / 4096 * V_FULLSCALE;
            double i_double = (double) current / 4096 * I_FULLSCALE / R_SENSE;

            double watt = v_double * i_double;
            power_sensor[sensor_index] = watt;

            /*if (master->log_enabled) {
                ss_mod3 << times[sensor] << "\t" << (int) sensor
                                         << "\t" << v_double << "\t" << i_double
                                         << "\t" << watt << std::endl;
            }*/
            times[sensor]++;

            sensor_index++;
            if (sensor_index == master->active_sensors) {
                sensor_index = 0;
                //if (master->log_enabled){
                //    (*(master->log_stream)) << ss_mod3.rdbuf();}

                double sum = std::accumulate(power_sensor, power_sensor + master->active_sensors, 0.0);
                master->power_total += sum;
                if (sum > master->power_max)
                    master->power_max = sum;
                
                master->sampled_instants++;
                
                master->last_power_read[0].voltage = v_double;
                master->last_power_read[0].current = i_double;
                master->last_power_read[0].power = sum;
                
                master->measure_list[master->current_measure].voltage = master->last_power_read[0].voltage;
                master->measure_list[master->current_measure].current = master->last_power_read[0].current;
                master->measure_list[master->current_measure].power   = master->last_power_read[0].power;
                master->current_measure++;
            }

            //STOP READ ACTIVE_SENSORS
            if (master->stop_task) break;
        }
    }
    
    /*master->Reset();
    ::tcflush(master->pw_file_descriptor, TCIOFLUSH);
    ::tcsetattr(master->pw_file_descriptor, TCSANOW, &master->pw_old_config);
    ::close(master->pw_file_descriptor);*/
}
