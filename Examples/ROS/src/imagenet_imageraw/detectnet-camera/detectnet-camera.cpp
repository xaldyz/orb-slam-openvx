/*
 * Copyright (c) 2017, NVIDIA CORPORATION. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "gstCamera.h"

#include "glDisplay.h"
#include "glTexture.h"

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "cudaMappedMemory.h"
#include "cudaNormalize.h"
#include "cudaFont.h"

#include "detectNet.h"

#include <chrono>
#include <fstream>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>

#include "powermon.hpp"
		
detectNet* net;
bool signal_recieved = false;
float confidence = 0.0f;
ros::Publisher pub;
int counter;

std::ofstream outfile;

double maxPower = -1;
double powerTotal = 0;
double numberOfSamples = 0;
long long totaltimesys = 0;
long long totaltimedetect = 0;
int numimages = 0;

std::chrono::steady_clock::time_point global_start;
/*--------------------------------------------------
--------------------- CALLBACK ---------------------
--------------------------------------------------*/

void shutdown(const std_msgs::String::ConstPtr& msg)
{
    ros::shutdown();
}

void frame_callback(const sensor_msgs::ImageConstPtr &msg)
{
	//std::cout << std::endl << "-------------------------- RECEIVED --------------------------" << std::endl << std::endl;

	// --------------------------------------------- BRG2RGBA ---------------------------------------------
	auto function_start = std::chrono::steady_clock::now();
	cv_bridge::CvImageConstPtr cv_ptr;
	try
	{
		cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}
   	cv::Mat frame = cv_ptr->image;

	//const int WIDTH = frame.cols;
    const int HEIGHT = frame.rows;
    const int WIDTH = HEIGHT;
    
	const uint32_t maxBoxes = net->GetMaxBoundingBoxes();
	//printf("maximum bounding boxes:  %u\n", maxBoxes);
	const uint32_t classes  = net->GetNumClasses();
	
	float* bbCPU    = NULL;
	float* bbCUDA   = NULL;
	float* confCPU  = NULL;
	float* confCUDA = NULL;
	
	if( !cudaAllocMapped((void**)&bbCPU, (void**)&bbCUDA, maxBoxes * sizeof(float4)) ||
	    !cudaAllocMapped((void**)&confCPU, (void**)&confCUDA, maxBoxes * classes * sizeof(float)) )
	{
		printf("detectnet-console:  failed to alloc output memory\n");
		return;
	}
	
  auto detect_start = std::chrono::steady_clock::now();
 
	size_t sz_img = WIDTH*HEIGHT*sizeof(float4);
	cv::Mat m2;
	cv::cvtColor(frame, m2, CV_BGR2RGBA, 4);
	m2 = m2(cv::Rect((frame.cols-WIDTH)/2, 0, WIDTH, HEIGHT)).clone();
	
	uchar* camData = new uchar[sz_img];
	cv::Mat frame_crop;
	cv::Mat continuousRGBA(HEIGHT, WIDTH, CV_32FC4, camData);
	m2.convertTo(continuousRGBA, CV_32FC4);

	float* data = (float*)camData;//continuousRGBA.data;
	float* im_RGBA;

	cudaMalloc(&im_RGBA, sz_img);
	// cudaMemcpy(im_RGBA, data, sz_img, cudaMemcpyHostToDevice);
	cudaMemcpy(im_RGBA, camData, sz_img, cudaMemcpyHostToDevice);
	std::cout << m2.isContinuous() << std::endl;
	//cv::rectangle(m2, cv::Point(10,10),cv::Point(40,40),cv::Scalar(255,0,0,255),2,8,0);

	int numBoundingBoxes = maxBoxes;
    

    bool b = net->Detect(im_RGBA, WIDTH, HEIGHT, bbCPU, &numBoundingBoxes, confCPU);

    //auto detect_end = std::chrono::steady_clock::now();
	//if( b && pub.getNumSubscribers()>0)
	if(true)
	{
		//printf("%i bounding boxes detected\n", numBoundingBoxes);
		int lastClass = 0;
		int lastStart = 0;
		
		for( int n=0; n < numBoundingBoxes; n++ )
		{
			const int nc = confCPU[n*2+1];
			float* bb = bbCPU + (n * 4);
			
			//printf("bounding box %i   (%f, %f)  (%f, %f)  w=%f  h=%f\n", n, bb[0], bb[1], bb[2], bb[3], bb[2] - bb[0], bb[3] - bb[1]);
			
			// cv::imwrite("/home/nvidia/catkin_ws/test1.png" , frame);
			cv::rectangle(m2, cv::Point(bb[0],bb[1]),cv::Point(bb[2],bb[3]),cv::Scalar(255,0,0,255),2,8,0);
		}
 		//cv::imwrite("/home/nvidia/catkin_ws/test.png" , frame);
		
		cv_bridge::CvImage img_bridge;
		sensor_msgs::Image img_msg; 

		std_msgs::Header header; 
		header.seq = counter; // user defined counter
		header.stamp = ros::Time::now(); // time
		img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGBA8, m2);
		img_bridge.toImageMsg(img_msg); // from cv_bridge to sensor_msgs::Image
		if(ros::ok()) pub.publish(img_msg);
		counter ++;//*/
		//cv::imwrite(std::string("/home/nvidia/git/orb-slam-openvx/Examples/ROS/devel/lib/imagenet_imageraw/img/") + std::to_string(counter) + std::string(".jpg"),m2);
	}
	cudaFree(im_RGBA);	
	delete[] camData;
	
	auto global_end = std::chrono::steady_clock::now();
	
	long long timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(global_end-global_start).count();
	long long detected_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(global_end-detect_start).count();
	long long function_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(global_end-function_start).count();
	outfile << cv_ptr->header.seq << ";" << timestamp << ";" << detected_elapsed << ";" << function_elapsed << ";-2" << std::endl;
	
	totaltimesys += function_elapsed;
	totaltimedetect += detected_elapsed;
	
	numimages++;
}


/*--------------------------------------------------
----------------- SIGNAL HANDLER -------------------
--------------------------------------------------*/

void sig_handler(int signo)
{
	if( signo == SIGINT )
	{
		printf("received SIGINT\n");
		signal_recieved = true;
	}
}


/*--------------------------------------------------
----------------------- MAIN -----------------------
--------------------------------------------------*/

int main( int argc, char** argv )
{
	// ------------------------------------------ CREATE IMAGENET ------------------------------------------
	counter = 0;
	net = detectNet::Create(argc, argv);
	
	if( !net )
	{
		printf("detectnet-camera:   failed to initialize imageNet\n");
		return 0;
	}

	// ------------------------------------------------------------------------------------------------------

	// ------------------------------------------------ ROS -------------------------------------------------	

	ros::init(argc, argv, "imagenet_camera");
    ros::NodeHandle nh("~");
	ros::AsyncSpinner spinner(1);
	std::string topic;
	bool log;
	nh.param<std::string>("receive_topic", topic, "/camera/image_raw");
	nh.param<bool>("log_power", log, false);
    ros::Subscriber sub = nh.subscribe<sensor_msgs::Image>(topic, 1, frame_callback);
    ros::Subscriber shutdown_sub = nh.subscribe(std::string("/shutdown"), 1, shutdown);
	pub = nh.advertise<sensor_msgs::Image>("camera/detected", 1);
	
	std::string fname;
	nh.param<std::string>("logfile", fname, "./logfile.txt");
	std::cout << "Opening " << fname << " for output" << std::endl;
	outfile.open(fname);
	if(!outfile.is_open())
	{
	    ROS_ERROR("error: file %s not opened!", fname.c_str());
	    ros::shutdown();
	    exit(1);
    }
    else
    {
        std::cout << fname << " opened and ready for output" << std::endl;
    }
	//outfile << "Frame number;Global time;Detected_time" << std::endl;
	
	if(log)
	{
	    std::ofstream file_internal;
        std::ofstream file_external;
        file_internal.open ("/home/nvidia/internal.txt", std::ofstream::out);
        file_external.open ("/home/nvidia/external.txt", std::ofstream::out);
        PowermonInternal p1(&file_internal);
        PowermonExternal p2(&file_external);
        std::chrono::time_point<std::chrono::steady_clock> t = std::chrono::steady_clock::now();
        p1.start(t);
        p2.start(t);
        global_start = std::chrono::steady_clock::now();
        ros::spin();
        p1.stop();
        p2.stop();
	} else {
    	global_start = std::chrono::steady_clock::now();
    	ros::spin();
	}
        
	/*while(ros::ok()){
		ros::spinOnce();
	}*/
	
	outfile.close();
	
	std::cout << "Stopping and reading power" << std::flush;
	
    std::cout << " OK " << std::endl << std::flush;

	ros::shutdown();
	
	
	
	/*double maxPower = -1;
    double powerTotal = 0;
    double numberOfSamples = 0;
    maxPower = p.getPowerMax();
    powerTotal += p.getPowerTotal();
    numberOfSamples += p.getSampledInstants();
    outfile.open(fname + std::string(".power"));
    outfile << "-------" << endl << endl;
    outfile << "TOTALTIME:" << totaltimesys << endl;
    outfile << "DETECTTIME:" << totaltimedetect << endl;
    if(numimages == 0) outfile << "DETECTTIME_AVG:-1" << endl;
    else               outfile << "DETECTTIME_AVG:" << totaltimedetect*1.0/numimages / 1000000000<< endl;
    if(numimages == 0) outfile << "TOTALTIME_AVG:-1" << endl;
    else               outfile << "TOTALTIME_AVG:" << totaltimesys*1.0/numimages / 1000000000<< endl;
    outfile << "MAXPOWER:" << maxPower << endl;
    outfile << "AVGENERGY:" << powerTotal * 1.0 / numberOfSamples << endl;
    outfile << "ENERGY:" << std::fixed << powerTotal << endl;
    outfile << "NUMSAMPLES:" << numberOfSamples << endl;
    outfile << "NUM_IMAGES:" << numimages << endl;
    outfile << "TRUE_ENERGY:" << powerTotal * 1.0 / numberOfSamples * totaltimedetect / 1000000000<< endl;
    */
    outfile.close();


	// ------------------------------------------------------------------------------------------------------
	
	printf("detectnet-camera\n  args (%i):  ", argc);

	for( int i=0; i < argc; i++ )
		printf("%i [%s]  ", i, argv[i]);
		
	printf("\n\n");

	// --------------------------------------- ATTACH SIGNAL HANDLER ----------------------------------------
	
	if( signal(SIGINT, sig_handler) == SIG_ERR )
		printf("\ncan't catch SIGINT\n");

	// ------------------------------------------------------------------------------------------------------

	return 0;
}

