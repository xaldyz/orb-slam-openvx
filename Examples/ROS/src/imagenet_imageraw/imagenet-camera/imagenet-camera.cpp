/*
 * Copyright (c) 2017, NVIDIA CORPORATION. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "gstCamera.h"
#include "glDisplay.h"
#include "glTexture.h"
#include <image_transport/image_transport.h>

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string>

#include "cudaNormalize.h"
#include "cudaFont.h"
#include "imageNet.h"


#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>

#define WIDTH 640
#define HEIGHT 480

imageNet* net;
cudaFont* font;

bool signal_recieved = false;
float confidence = 0.0f;

/*--------------------------------------------------
--------------------- CALLBACK ---------------------
--------------------------------------------------*/

void shutdown(const std_msgs::String::ConstPtr& msg)
{
    ros::shutdown();
}

void frame_callback(const sensor_msgs::ImageConstPtr &msg)
{
	std::cout << std::endl << "-------------------------- RECEIVED --------------------------" << std::endl << std::endl;

	// --------------------------------------------- BRG2RGBA ---------------------------------------------
	
	cv_bridge::CvImageConstPtr cv_ptr;
	try
	{
		//cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);
		cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::MONO8);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}
   	cv::Mat frame = cv_ptr->image;
   	std::cout << "Channels: " << frame.channels() << std::endl;

	//cv::Mat frame(HEIGHT, WIDTH, CV_8UC4, cv::Scalar(0, 255, 0, 255)); //BGRA
	//cv::imwrite("/home/nvidia/catkin_ws/Imm.jpg", frame);
	size_t sz_img = WIDTH*HEIGHT*sizeof(float4);
	cv::Mat m2;
	frame.convertTo(m2, CV_32FC1);
    //cv::cvtColor(frame, m2, CV_BGRA2RGBA, 4);

	//cv::imwrite("/home/nvidia/catkin_ws/Imm3.jpg", m2);
	uchar* camData = new uchar[sz_img];
	cv::Mat continuousRGBA(HEIGHT, WIDTH, CV_32FC4, camData);
	//m2.convertTo(continuousRGBA, CV_32FC1);
	cv::cvtColor(m2, continuousRGBA, CV_GRAY2RGBA);
	//cv::imshow("Image", continuousRGBA);
	std::cout << continuousRGBA.channels() << std::endl;
	cv::imwrite("./Imm2.jpg", continuousRGBA);
    std::cout << continuousRGBA.isContinuous() << std::endl;
	// ----------------------------------------------------------------------------------------------------

	float* data = (float*)continuousRGBA.data;//camData
	float* im_RGBA;
	// ------------------------------------------ CLASSIFICATION -------------------------------------------

	cudaMalloc(&im_RGBA, sz_img);
	// cudaMemcpy(im_RGBA, data, sz_img, cudaMemcpyHostToDevice);
	cudaMemcpy(im_RGBA, data, sz_img, cudaMemcpyHostToDevice);

	
	const int img_class = net->Classify(im_RGBA, WIDTH, HEIGHT, &confidence);

	cudaFree(im_RGBA);

	if( img_class >= 0 )
		printf("imagenet-camera:  %2.5f%% class #%i (%s)\n", confidence * 100.0f, img_class, net->GetClassDesc(img_class));

	delete[] camData;
}
		


/*--------------------------------------------------
----------------- SIGNAL HANDLER -------------------
--------------------------------------------------*/

void sig_handler(int signo)
{
	if( signo == SIGINT )
	{
		printf("received SIGINT\n");
		signal_recieved = true;
		exit(1);
	}
}



/*--------------------------------------------------
----------------------- MAIN -----------------------
--------------------------------------------------*/

int main( int argc, char** argv )
{
	// ------------------------------------------ CREATE IMAGENET ------------------------------------------
	//cv::namedWindow("Image");
	net = imageNet::Create(argc, argv);
	
	if( !net )
	{
		printf("imagenet-console:   failed to initialize imageNet\n");
		return 0;
	}else printf("OK");
    
	// ------------------------------------------------------------------------------------------------------

	// ------------------------------------------------ ROS -------------------------------------------------

	
	ros::init(argc, argv, "imagenet_camera");
    ros::NodeHandle nh;
	ros::AsyncSpinner spinner(1);
	std::string topic;
	nh.param<std::string>("receive_topic", topic, "/camera/image_raw");
    //ros::Subscriber sub = nh.subscribe<sensor_msgs::Image>("csi_cam/image_raw", 1000, frame_callback);
    ros::Subscriber sub = nh.subscribe<sensor_msgs::Image>(topic, 1, frame_callback);
    ros::Subscriber shutdown_sub = nh.subscribe(std::string("/shutdown"), 1, shutdown);
	
	ros::spin();
	//while(ros::ok()){
	//for(int i = 0; i < 100;++i)
	//ros::spinOnce();
    //}

	// ------------------------------------------------------------------------------------------------------

	printf("imagenet-camera\n  args (%i):  ", argc);

	for( int i=0; i < argc; i++ )
		printf("%i [%s]  ", i, argv[i]);
		
	printf("\n\n");
	

	// --------------------------------------- ATTACH SIGNAL HANDLER ----------------------------------------
	
	if( signal(SIGINT, sig_handler) == SIG_ERR )
		printf("\ncan't catch SIGINT\n");
	
	// ------------------------------------------------------------------------------------------------------

	return 0;
}

