// Parameters

// send_topic: the topic to publish images (default /camera/image_still)
// image_file: the path to the image file to be published


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>

using namespace std;

void shutdown(const std_msgs::String::ConstPtr& msg)
{
    ros::shutdown();
}

void dummy(const sensor_msgs::ImageConstPtr& msg)
{

}

int main(int argc, char **argv)
{
    //if(argc != 2)
    //{
    //    cerr << endl << "Usage: ./ros_mono_kitti_source path_to_sequence" << endl;
    //    return 1;
    //}

    ros::init(argc, argv, "dummy");
    ros::start();
    
    ros::NodeHandle nodeHandler("~");
    
    std::string camera_topic;
    nodeHandler.param<std::string>("receive_topic", camera_topic, "/camera/image_raw");
    
    std::string topic;
    std::string image;
    ros::Subscriber image_subscriber = nodeHandler.subscribe(camera_topic.c_str(), 1, dummy);
    ros::Subscriber shutdown_sub = nodeHandler.subscribe(std::string("/shutdown"), 1, shutdown);
    
    ros::spin();
    
    ros::shutdown();

    return 0;
}


