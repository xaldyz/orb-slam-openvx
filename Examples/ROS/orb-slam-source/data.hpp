#pragma once

#include <vector>
#include <string>
#include "System.h"
#include <ros/ros.h>

typedef struct {
	int    frame_offset = 0;
	double wait_time = 0;
	
	//state
	double time = 0;
	
	//constant
	int frames_to_advance = 1;
	bool realtime = true;
	bool skip = true;
} simulation_time_status;

typedef struct {
    simulation_time_status s;
    double fps_multiplier = 1;
    bool include_img_load = true;
    std::string camera_topic = "";
    std::string query_topic = "";
    std::string response_topic = "";
    
    std::string vocabulary_file = "";
    std::string settings_file = "";
    std::string sequence_path = "";
    std::string times_file = "";
} mono_params;


void LoadImages(mono_params p, vector<string> &vstrImageFilenames,
                vector<double> &vTimestamps);

mono_params ParseArgs(ros::NodeHandle nodeHandle);

void SaveSLAM(ORB_SLAM2::System& SLAM, mono_params p);
