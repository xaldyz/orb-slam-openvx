#include<iostream>
#include<algorithm>
#include<fstream>
#include<iomanip>
#include<mcheck.h>
#include<sstream>
#include<string.h>
#include <ros/ros.h>

using namespace std;

#include "data.hpp"

   
mono_params ParseArgs(ros::NodeHandle nodeHandle)
{
    mono_params p;
    std::string camera_topic;
    std::string query_topic;
    std::string response_topic;
    std::string vocabulary_file;
    std::string settings_file;

    // TODO rinominare receive_topic in camera_topic prima o poi! (anche nei .launch)
    nodeHandle.param<std::string>("receive_topic", p.camera_topic, "/camera/image_raw");
    nodeHandle.param<std::string>("query_topic", p.query_topic, "/orbslam2/query");
    nodeHandle.param<std::string>("response_topic", p.response_topic, "/orbslam2/response");
    nodeHandle.param<std::string>("vocabulary_file", p.vocabulary_file, "../../../Vocabulary/ORBvoc.txt");
    nodeHandle.param<std::string>("settings_file", p.settings_file, "../../Monocular/KITTI00-02.yaml");

    return p;
}

void SaveSLAM(ORB_SLAM2::System& SLAM, mono_params p)
{
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt", p.fps_multiplier);
    SLAM.SaveTrajectoryKITTI("Trajectory.txt", p.fps_multiplier);
}

void LoadImages(mono_params p, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
{
    ifstream fTimes;
    string strPathTimeFile = p.sequence_path + "/times.txt";
    fTimes.open(strPathTimeFile.c_str());
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            ss >> t;
            vTimestamps.push_back(t/p.fps_multiplier);
        }
    }

    string strPrefixLeft = p.sequence_path + "/image_0/";

    const int nTimes = vTimestamps.size();
    vstrImageFilenames.resize(nTimes);

    for(int i=0; i<nTimes; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        vstrImageFilenames[i] = strPrefixLeft + ss.str() + ".png";
    }
}
