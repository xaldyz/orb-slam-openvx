/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>
#include<mcheck.h>

#include<opencv2/core/core.hpp>

#include"System.h"
#include"MapPoint.h"

#include "data.hpp"

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>
#include "pcl_ros/point_cloud.h"
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

using namespace std;
using namespace ORB_SLAM2;

typedef
#ifdef COMPILEDWITHC11
std::chrono::steady_clock
#else
std::chrono::monotonic_clock
#endif
chrono_orb
;

long long totaltimesys = 0;

class ImageGrabber
{
    bool started;
    chrono_orb::time_point time_start;
public:

    ImageGrabber(ORB_SLAM2::System* pSLAM):mpSLAM(pSLAM){ started = false; }

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);
    void GrabStereo(const sensor_msgs::ImageConstPtr& left, const sensor_msgs::ImageConstPtr& right);
    void AnswerQuery(const std_msgs::String::ConstPtr& msg);
    void Shutdown(const std_msgs::String::ConstPtr& msg);
    
    void setPublisher(ros::Publisher pub);
    void setPublisherTest(ros::Publisher pub);

    ORB_SLAM2::System* mpSLAM;
    ros::Publisher ros_pub;
    ros::Publisher map_pub;
    pcl::PointCloud<pcl::PointXYZ> cloud_points;
};


int main(int argc, char **argv)
{
    ros::init(argc, argv, "Mono");
    ros::start();
    ros::NodeHandle nodeHandler("~");
    
    mono_params params = ParseArgs(nodeHandler);
    
    simulation_time_status s = params.s;
    
    std::ofstream file_internal;
    std::ofstream file_external;
    file_internal.open ("/home/nvidia/internal.txt", std::ofstream::out);
    file_external.open ("/home/nvidia/external.txt", std::ofstream::out);
    PowermonInternal p1(&file_internal);
    PowermonExternal p2(&file_external);
    
    Powermon* p[2] = {&p1, &p2};
    
    double maxPower = -1;
    double powerTotal = 0;
    double numberOfSamples = 0;
    //mtrace();
    {
        bool monocular = true;
        nodeHandler.param<bool>("monocular", monocular, true);
        
        std::vector<long long> times;
        ORB_SLAM2::System SLAM(params.vocabulary_file,params.settings_file,((monocular) ? ORB_SLAM2::System::MONOCULAR : ORB_SLAM2::System::STEREO),true);
        
        ImageGrabber igb(&SLAM);
        ros::Subscriber shutdown_sub = nodeHandler.subscribe(std::string("/shutdown"), 1000, &ImageGrabber::Shutdown, &igb);
        
        ros::Subscriber query_sub = nodeHandler.subscribe(params.query_topic.c_str(), 1000, &ImageGrabber::AnswerQuery, &igb);
        ros::Publisher query_pub = nodeHandler.advertise<std_msgs::String>(params.response_topic.c_str(), 1000);

        igb.setPublisher(query_pub);
        
        ros::Publisher map_pub = nodeHandler.advertise<pcl::PointCloud<pcl::PointXYZ>>(nodeHandler.resolveName("TEST_PCL"), 1000);
        igb.setPublisherTest(map_pub);
        

        chrono_orb::time_point sysstart;
        long long time_tck = 0;
        
        if(!monocular)
        {
            std::cout << "Subscribing to stereo..." << std::endl << std::flush;
            message_filters::Subscriber<sensor_msgs::Image> left_sub(nodeHandler, "/camera/left/image_raw", 1);
            message_filters::Subscriber<sensor_msgs::Image> right_sub(nodeHandler, "/camera/right/image_raw", 1);
            typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
            message_filters::Synchronizer<sync_pol> sync(sync_pol(10), left_sub,right_sub);
            sync.registerCallback(boost::bind(&ImageGrabber::GrabStereo,&igb,_1,_2));
            
            chrono_orb::time_point t = chrono_orb::now();
            
            p[1]->start(t);
            p[0]->start(t);

            sysstart = chrono_orb::now();
            
            ros::spin();        

            p[0]->stop();
            p[1]->stop();
        } else {
            std::cout << "Subscribing to monocular..." << std::endl << std::flush;
            ros::Subscriber image_subscriber = nodeHandler.subscribe(params.camera_topic.c_str(), 1, &ImageGrabber::GrabImage,&igb);
            std::cout << "Subscribing OK!" << std::endl;
            
            chrono_orb::time_point t = chrono_orb::now();
            p[1]->start(t);
            p[0]->start(t);

            sysstart = chrono_orb::now();
            
            ros::spin();        

            p[0]->stop();
            p[1]->stop();
        }
        
        chrono_orb::time_point sysend = chrono_orb::now();
        
        // Stop all threads
        SLAM.Shutdown();
        
        // Tracking time statistics
        // Save camera trajectory
        SaveSLAM(SLAM, params);
        
        time_tck = std::chrono::duration <long long, nano> (sysend - sysstart).count();

        /*        
        double maxPower = -1;
        double powerTotal = 0;
        double numberOfSamples = 0;
        maxPower = p.getPowerMax();
        powerTotal += p.getPowerTotal();
        numberOfSamples += p.getSampledInstants();
        


        cout << "-------" << endl << endl;
        cout << "TOTALTIME:" << totaltimesys << endl;
        cout << "MAXPOWER:" << maxPower << endl;
        cout << "AVGENERGY:" << powerTotal * 1.0 / numberOfSamples << endl;
        cout << "ENERGY:" << std::fixed << powerTotal << endl;
        cout << "NUMSAMPLES:" << numberOfSamples << endl;
        cout << "TRUE_ENERGY:" << powerTotal * 1.0 / numberOfSamples * time_tck / 1000000000<< endl;
        cout << "NUMIMAGES:" << endl;
        cout << "TIMETRACKING:" << endl;*/
        //cout << "NUMIMAGES:" << nImages << endl;
        //cout << "TIMETRACKING:" << time_tck/1000000000.0 << endl;
    }

    
    return 0;
}


void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg)
{
    if(!started) {
        started = true;
        time_start = chrono_orb::now();
    }
    chrono_orb::time_point sysstart = chrono_orb::now();
    
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptr;
    //std::cout << "GrabImage!\n";
    try
    {
        cv_ptr = cv_bridge::toCvShare(msg);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    
    mpSLAM->TrackMonocular(cv_ptr->image,
    cv_ptr->header.stamp.toSec(),
    std::chrono::duration <double, milli> (sysstart - time_start).count(),
    cv_ptr->header.seq);//number of frame skipped. ATM, it's no use
    
    chrono_orb::time_point sysend = chrono_orb::now();
    
    totaltimesys += std::chrono::duration <long long, nano> (sysend - sysstart).count();
}

void ImageGrabber::GrabStereo(const sensor_msgs::ImageConstPtr& left, const sensor_msgs::ImageConstPtr& right)
{
    if(!started) {
        started = true;
        time_start = chrono_orb::now();
    }
    chrono_orb::time_point sysstart = chrono_orb::now();
    
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr left_ptr;
    cv_bridge::CvImageConstPtr right_ptr;
    //std::cout << "GrabImage!\n";
    try
    {
        left_ptr = cv_bridge::toCvShare(left);
        right_ptr = cv_bridge::toCvShare(right);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    
    mpSLAM->TrackStereo(left_ptr->image, right_ptr->image,
    left_ptr->header.stamp.toSec(),
    std::chrono::duration <double, milli> (sysstart - time_start).count(),
    left_ptr->header.seq);//number of frame skipped. ATM, it's no use
    
    chrono_orb::time_point sysend = chrono_orb::now();
    
    totaltimesys += std::chrono::duration <long long, nano> (sysend - sysstart).count();
}

void ImageGrabber::Shutdown(const std_msgs::String::ConstPtr& msg) {
    // EVERY message will cause a shutdown!!
    std::cout << "Received SHUTDOWN!" << std::endl;
    ros::shutdown();
}

void ImageGrabber::AnswerQuery(const std_msgs::String::ConstPtr& msg) {
    std::string query = msg->data;

    std::cout << "Answering query id:"  << query << std::endl;

    // idea, basing of query value... do something
    // maybe using a structured field, with a requestid

    // for now... sending if there is a huge map change
    bool isMapChanged = this->mpSLAM->MapChanged();

    std::string res = string("Mapchanged =") + (isMapChanged?"true":"false");

    this->ros_pub.publish(res);
    
    //if(this->map_pub.getNumSubscribers() > 0)
    {
        cloud_points.points.clear();
        cloud_points.header.frame_id = "base_tf";    
        const vector<MapPoint*> vpMPs = mpSLAM->GetAllMapPoints();
        //const vector<MapPoint*> vpMPs = mpSLAM->GetTrackedMapPoints();
        
        bool bb = true;
        for(size_t i=0; i<vpMPs.size(); i++)
        {
            MapPoint* pMP = vpMPs[i];
            if(pMP == NULL) continue;

            if(pMP->isBad())
                continue;
            cv::Mat wp = pMP->GetWorldPos();
            if(bb)
            {
                bb = false;
    //            std::cerr << "[MAP] POINT: " << wp << ", x = " << wp.at<float>(0,0) << std::endl;
            }
        
            cloud_points.points.push_back(pcl::PointXYZ(wp.at<float>(0,0),wp.at<float>(2,0),wp.at<float>(1,0)));
        }
        this->map_pub.publish(cloud_points.makeShared());
    }
}

void ImageGrabber::setPublisher(ros::Publisher pub) {
    this->ros_pub = pub;
}

void ImageGrabber::setPublisherTest(ros::Publisher pub) {
    this->map_pub = pub;
}

