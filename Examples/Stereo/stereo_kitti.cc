#include<iostream>
#include<algorithm>
#include<fstream>
#include<iomanip>
#include<mcheck.h>
#include<sstream>
#include<string.h>

using namespace std;

#include "data.hpp"


bool CheckMain(int argc, char **argv)
{
    if(argc < 4)
    {
        cerr << endl << "Usage: ./stereo_kitti path_to_vocabulary path_to_settings path_to_sequence [FPS multiplier] [Frames to skip] [skip type]" << endl;
        return false;
    }
    return true;
}
    
mono_params ParseArgs(int argc, char **argv)
{
    mono_params p;
    p.s.frames_to_advance = 1;
    p.s.time = 0;
    p.s.realtime = true;
	p.s.skip     = false;
	p.include_img_load = true;
    p.fps_multiplier = 1;
    
    p.voc_file = argv[1];
    p.settings_file = argv[2];
    p.sequence_path1 = argv[3];
    p.sequence_path2 = argv[3];
    
    if(argc >= 5)
    {
        p.fps_multiplier = atof(argv[4]);
    }
    if(argc >= 6)
    {
        p.s.frames_to_advance = atoi(argv[5])+1;
    }
    
    if(argc >= 7)
    {
        if(strcmp(argv[6], "skip") == 0)
        {
            p.s.skip = true;
        }
        else if(strcmp(argv[6], "skip_noimgtime") == 0)
        {
            p.s.skip = true;
            p.include_img_load = false;
        }
    }
    
    return p;
}

void SaveSLAM(ORB_SLAM2::System& SLAM, mono_params p)
{
//    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");
}

void LoadImages(mono_params p, vector<string> &vstrImageFilenames1, vector<string> &vstrImageFilenames2, vector<double> &vTimestamps)
{
{
    ifstream fTimes;
    string strPathTimeFile = p.sequence_path1 + "/times.txt";
    fTimes.open(strPathTimeFile.c_str());
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            ss >> t;
            vTimestamps.push_back(t/p.fps_multiplier);
        }
    }

    string strPrefixLeft = p.sequence_path1 + "/image_0/";

    const int nTimes = vTimestamps.size();
    vstrImageFilenames1.resize(nTimes);

    for(int i=0; i<nTimes; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        vstrImageFilenames1[i] = strPrefixLeft + ss.str() + ".png";
    }
}
{
    string strPrefixLeft = p.sequence_path2 + "/image_1/";

    const int nTimes = vTimestamps.size();
    vstrImageFilenames2.resize(nTimes);

    for(int i=0; i<nTimes; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        vstrImageFilenames2[i] = strPrefixLeft + ss.str() + ".png";
    }
}
}
