#pragma once

#include <vector>
#include <string>
#include "System.h"

typedef struct {
	int    frame_offset = 0;
	double wait_time = 0;
	
	//state
	double time = 0;
	
	//constant
	int frames_to_advance = 1;
	bool realtime = true;
	bool skip = true;
} simulation_time_status;

typedef struct {
    simulation_time_status s;
    double fps_multiplier = 1;
    bool include_img_load = true;
    std::string voc_file = "";
    std::string settings_file = "";
    std::string sequence_path1 = "";
    std::string times_file1 = "";
    std::string sequence_path2 = "";
    std::string times_file2 = "";
} mono_params;

void LoadImages(mono_params p, vector<string> &vstrImageFilenames1, vector<string> &vstrImageFilenames2,
                vector<double> &vTimestamps);
bool CheckMain(int argc, char **argv);
mono_params ParseArgs(int argc, char **argv);

void SaveSLAM(ORB_SLAM2::System& SLAM, mono_params p);
