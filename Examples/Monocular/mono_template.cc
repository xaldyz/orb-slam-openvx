/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>
#include<mcheck.h>

#include<opencv2/core/core.hpp>

#include"System.h"

#include "data.hpp"

using namespace std;

/*
Legend:
  - |   Frame to be processed
  - -   Time between events
  - :   Frame to be skipped (to lower the framerate)
  - s   starting processing
  - e   ending processing
  - w   wait
  - *   continue elaboration
  - +   Time of frame to be processed but elaboration ongoing
  
Example (output FPS halved)
 > |----:----|----:----|----:----|----:----|

Case 1
  > s---ewwwwws********
   State:
  	 delay 0
   Out:
   	frames_offset 0 (able to process all)
    wait time     5
    delay         0
  	
Case 2
  > s---------+--es*****
   State:
  	 delay 0
   Out:
  	 frames_offset 0 (able to process all)
  	 wait time     0
  	 delay         3 (frame processing started 3 unit later)

Case 3
  > s---------+---------+-es******
   State:
  	 delay 0
   Out:
  	 frames_offset 1 (Missed completely a frame)
  	 wait time     0
  	 delay         2 (frame processing started 2 unit later)
*/

void simulation_step(double time_processed, vector<double> vTimestamps, int start_idx, simulation_time_status& s)
{
	s.frame_offset = 0;
	s.wait_time    = 0;
	
	int n_images = vTimestamps.size();
	int next_idx = start_idx + s.frames_to_advance;
	
	double start_time = s.time;//vTimestamps[start_idx];
	double final_time_processed = start_time + time_processed;
	
	if(next_idx >= n_images)
	{
		// End of the stream: special handling
		s.frame_offset = 0;
		s.wait_time = (s.skip) ? std::max(0.0, vTimestamps[n_images-1] - final_time_processed) : 0.0;//wait the complete acquisition
		s.time += time_processed + s.wait_time;
		return;
	}
	
	if(!s.skip)
	{
		//no skip frame, only wait if processing time was short enough
		s.wait_time = std::max(0.0, vTimestamps[next_idx] - ( vTimestamps[start_idx] + time_processed )); 
		//std::cout << "Wait time: " << s.wait_time << " " << vTimestamps[next_idx] << " " << vTimestamps[start_idx] << " " << time_processed << std::endl;
		s.time += time_processed + s.wait_time;
		return;
	}
	
	
	double next_time = vTimestamps[next_idx];
	if(final_time_processed <= next_time)
	{
		//case 1
		//processing did well, need to wait (THE ONLY CASE!) and no more delay!
		s.wait_time = next_time - final_time_processed;
		s.time += time_processed + s.wait_time;
	}
	else
	{
	    s.time += time_processed;
		//offset because do/while
		s.frame_offset = 0;
        double prev_time = vTimestamps[start_idx];
        double cur_time  = next_time;
        double delta_time = cur_time - prev_time;
		do
		{
			s.frame_offset += s.frames_to_advance;
			
			if(start_idx+1+s.frame_offset < n_images)
			    cur_time = vTimestamps[start_idx+1+s.frame_offset];
			else
			    cur_time = prev_time + delta_time;
			delta_time = cur_time - prev_time;
			prev_time  = cur_time;
		}while(cur_time < s.time);
		//recover a step if the time is not a perfect multiple
		//(if a perfect multiple, the execution finished when new frame is ready)
		if(cur_time != s.time) s.frame_offset -= s.frames_to_advance;
	}
}

#include<unistd.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
void print_free_ram(std::string s = "")
{
    struct sysinfo memInfo;
    sysinfo(&memInfo);
    long long totalVirtualMem = memInfo.totalram - memInfo.freeram;
    //totalVirtualMem += memInfo.totalswap - memInfo.freeswap;
    //totalVirtualMem *= memInfo.mem_unit;
    double ram = totalVirtualMem*memInfo.mem_unit;
    ram /= 1024.0;//KB
    ram /= 1024.0;//MB
    ram /= 1024.0;//GB
    std::cout << s << " - ram usage: " << ram << " GB" << std::endl;
}

typedef
#ifdef COMPILEDWITHC11
std::chrono::steady_clock
#else
std::chrono::monotonic_clock
#endif
chrono_orb
;

int main(int argc, char **argv)
{
    if(!CheckMain(argc, argv))
    {
        return 1;
    }
    
    mono_params params = ParseArgs(argc, argv);
    
    simulation_time_status s = params.s;
    
    double maxPower = -1;
    double powerTotal = 0;
    double numberOfSamples = 0;
    print_free_ram("Before everything");
    //mtrace();
    {
        std::vector<long long> times;
        ORB_SLAM2::System SLAM(params.voc_file,params.settings_file,ORB_SLAM2::System::MONOCULAR,false);
        std::ofstream file_internal;
        std::ofstream file_external;
        file_internal.open ("internal.txt");
        file_external.open ("external.txt");
        PowermonInternal p1(&file_internal);
        PowermonExternal p2(&file_external);
        
        Powermon* p[2] = {&p1, &p2};
        // = {p1, p2};

        // Retrieve paths to images
        vector<string> vstrImageFilenames;
        vector<double> vTimestamps;
        std::cout << "Loading images!" << std::endl << std::flush;
        LoadImages(params, vstrImageFilenames, vTimestamps);
        std::cout << "End images load!" << std::endl << std::flush;
        s.time = vTimestamps[0];

        int nImages = vstrImageFilenames.size();

        // Create SLAM system. It initializes all system threads and gets ready to process frames.
        std::cout << "Creating system!" << std::endl;
        
        std::cout << "End system create!" << std::endl;

        // Vector for tracking time statistics
        vector<float> vTimesTrack;
        vTimesTrack.resize(nImages);

        cout << endl << "-------" << endl;
        cout << "Start processing sequence ..." << endl;
        cout << "Images in the sequence: " << nImages << endl << endl;

        // Main loop
        cv::Mat im;      
        
        std::ofstream outfile("time.txt");

        chrono_orb::time_point sysstart = chrono_orb::now();
        
        p[0]->start();
        p[1]->start();

        //long long diffTime = 0;
        long long time_tck = 0;
        cv::Mat last_tcw = cv::Mat(0,0,CV_8UC1);
        
        //for(int nLoop = 0; nLoop < 1; nLoop++)
            for(int ni=0; ni<nImages; ni+=s.frames_to_advance)
            {
                //std::cout << "Frame " << ni+1 << "/" << nImages << std::endl;

                chrono_orb::time_point tt1 = chrono_orb::now();

                // Read image from file
                im = cv::imread(vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
                //im = cv::imread("/home/nvidia/Desktop/mh01/cam0/data/1403636579763555584.png",CV_LOAD_IMAGE_UNCHANGED);
                double tframe = vTimestamps[ni];

                if(im.empty())
                {
                    cerr << endl << "Failed to load image at: " << vstrImageFilenames[ni] << endl;
                    return 1;
                }
                
                if(!params.include_img_load)
                    tt1 = chrono_orb::now();

                chrono_orb::time_point start = chrono_orb::now();
                // Pass the image to the SLAM system
                //print_free_ram("Before ORB SLAM track");
                SLAM.TrackMonocular(im,tframe,std::chrono::duration_cast<std::chrono::duration<double> >(start - sysstart).count(),ni);
                //print_free_ram("After ORB SLAM track");
                chrono_orb::time_point end = chrono_orb::now();
                times.push_back(std::chrono::duration <long long, nano> (end - start).count());
                
                chrono_orb::time_point tt2 = chrono_orb::now();

                double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(tt2 - tt1).count();
                vTimesTrack[ni]=ttrack;

                time_tck += std::chrono::duration <long long, nano> (tt2-tt1).count();
                
	            // Wait to load the next frame
	            if(s.realtime)
	            {
			        simulation_step(ttrack, vTimestamps, ni, s);
			        ni += s.frame_offset;
			        if(s.frame_offset > 0)
			        {
			            std::cout << "Skipping " << s.frame_offset << " frames" << std::endl;
			        }
			        if(s.wait_time > 0)
	                	usleep(s.wait_time*1e6);
		        }


                SLAM.last_frame_profiled.time_since_beginning = std::chrono::duration<long long, nano>(start - sysstart).count();
                outfile
                << SLAM.last_frame_profiled.nframe << ";"
                << ttrack << ";"
                << SLAM.last_frame_profiled.timestamp << ";"
                << SLAM.last_frame_profiled.time_since_beginning_ms << ";"
                << SLAM.last_frame_profiled.time_since_beginning << ";"
                << SLAM.last_frame_profiled.time_complete_track << ";"
                << SLAM.last_frame_profiled.time_frame_construction << ";"
                << SLAM.last_frame_profiled.time_orb << ";"
                << SLAM.last_frame_profiled.time_stereo_matches << ";"
                << SLAM.last_frame_profiled.time_localization << ";"
                << SLAM.last_frame_profiled.state << ";"
                << SLAM.last_frame_profiled.tcw.rows << ";" << SLAM.last_frame_profiled.tcw.cols << ";";
                int i;
                if(SLAM.last_frame_profiled.state > 2) SLAM.last_frame_profiled.tcw = last_tcw;
                for(i = 0; i < SLAM.last_frame_profiled.tcw.rows * SLAM.last_frame_profiled.tcw.cols; ++i)
                {
                    switch(SLAM.last_frame_profiled.tcw.type()) {
                    case 0: outfile << SLAM.last_frame_profiled.tcw.at<uint8_t>(i/4, i%4) << ";"; break;
                    case 1: outfile << SLAM.last_frame_profiled.tcw.at<int8_t>(i/4, i%4) << ";"; break;
                    case 2: outfile << SLAM.last_frame_profiled.tcw.at<uint16_t>(i/4, i%4) << ";"; break;
                    case 3: outfile << SLAM.last_frame_profiled.tcw.at<int16_t>(i/4, i%4) << ";"; break;
                    case 4: outfile << SLAM.last_frame_profiled.tcw.at<int32_t>(i/4, i%4) << ";"; break;
                    case 5: outfile << SLAM.last_frame_profiled.tcw.at<float>(i/4, i%4) << ";"; break;
                    case 6: outfile << SLAM.last_frame_profiled.tcw.at<double>(i/4, i%4) << ";"; break;
                    default: outfile << ";";
                    }
                }
                for(; i < 4*4; ++i) { outfile << ";"; }
                outfile << std::endl;
                
                last_tcw = SLAM.last_frame_profiled.tcw;
                
            }

        p[0]->stop();
        p[1]->stop();

        // Stop all threads
        print_free_ram("Before ORB SLAM shutdown");
        SLAM.Shutdown();
        print_free_ram("After ORB SLAM shutdown");
        
        chrono_orb::time_point sysend = chrono_orb::now();
        long long totaltimesys = std::chrono::duration <long long, nano> (sysend - sysstart).count();

        // Tracking time statistics
        sort(vTimesTrack.begin(),vTimesTrack.end());
        float totaltime = 0;
        for(int ni=0; ni<nImages; ni++)
        {
            totaltime+=vTimesTrack[ni];
        }
        cout << "-------" << endl << endl;
        cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
        cout << "mean tracking time: " << totaltime/nImages << endl;

        // Save camera trajectory
        //SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");
        SaveSLAM(SLAM, params);
        

        //TOFIX
        if (false && !times.empty()) {
            // stampo su file times
            std::ofstream timesFile;
            timesFile.open ("timesTotalTracking.csv", std::ios_base::app);
            for(long long t : times)
            {
                timesFile << t << ";\n";
            }
            timesFile.close();
        }/*
        maxPower = p.getPowerMax();
        powerTotal = p.getPowerTotal();
        numberOfSamples = p.getSampledInstants();
        
        
        cout << "-------" << endl << endl;
        cout << "TOTALTIME:" << totaltimesys << endl;
        cout << "MAXPOWER:" << maxPower << endl;
        cout << "AVGENERGY:" << powerTotal * 1.0 / numberOfSamples << endl;
        cout << "ENERGY:" << powerTotal << endl;
        cout << "NUMSAMPLES:" << numberOfSamples << endl;
        cout << "TRUE_ENERGY:" << powerTotal * 1.0 / numberOfSamples * time_tck / 1000000000<< endl;
        cout << "NUMIMAGES:" << nImages << endl;
        cout << "TIMETRACKING:" << time_tck/1000000000.0 << endl;*/
        print_free_ram("Before ORB SLAM destruction");
    }
    print_free_ram("After ORB SLAM destruction");

    /*if(!times.empty())
    {
        std::ofstream timesFile;
        timesFile.open ("times.csv", std::ios_base::app);
        timesFile << "#Frame;Name Processing function;Level;Time spent (ns);Time spent (ms)" << std::endl;
        int frame = 0;
        for(long long t : times)
        {
            timesFile << frame++ << ";";
            timesFile << "Total system time;";
            timesFile << -1 << ";";
            timesFile << t  << ";";
            timesFile << t / 1000000.0 << ";";
            timesFile << std::endl;
        }
        timesFile.close();
    }*/
    //muntrace();
    return 0;
}
