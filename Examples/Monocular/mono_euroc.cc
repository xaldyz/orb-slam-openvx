#include<iostream>
#include<algorithm>
#include<fstream>
#include<iomanip>
#include<mcheck.h>
#include<sstream>
#include<string.h>

using namespace std;

#include "data.hpp"


bool CheckMain(int argc, char **argv)
{
    if(argc < 5)
    {
        cerr << endl << "Usage: ./mono_euroc path_to_vocabulary path_to_settings path_to_folder [FPS multiplier] [Frames to skip] [skip type]" << endl;
        return false;
    }
    return true;
}
    
mono_params ParseArgs(int argc, char **argv)
{
    mono_params p;
    p.s.frames_to_advance = 1;
    p.s.time = 0;
    p.s.realtime = true;
	p.s.skip     = false;
	p.include_img_load = true;
    p.fps_multiplier = 1;
    
    
    p.voc_file = argv[1];
    p.settings_file = argv[2];
    p.sequence_path = argv[3]; 
    p.times_file = p.sequence_path + "/cam0/data.csv";   
    p.sequence_path += "/cam0/data";
    
    if(argc >= 5)
    {
        p.fps_multiplier = atof(argv[4]);
    }
    if(argc >= 6)
    {
        p.s.frames_to_advance = atoi(argv[5])+1;
    }
    
    if(argc >= 7)
    {
        if(strcmp(argv[6], "skip") == 0)
        {
            p.s.skip = true;
        }
        else if(strcmp(argv[6], "skip_noimgtime") == 0)
        {
            p.s.skip = true;
            p.include_img_load = false;
        }
    }
    
    return p;
}

void SaveSLAM(ORB_SLAM2::System& SLAM, mono_params p)
{
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt", p.fps_multiplier);
}

void LoadImages(mono_params p, vector<string> &vstrImages, vector<double> &vTimeStamps)
{
    ifstream fTimes;
    fTimes.open(p.times_file.c_str());
    string s;
    getline(fTimes,s);
    vTimeStamps.reserve(5000);
    vstrImages.reserve(5000);
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss(s);
            double t;
            ss >> t;
            std::string(token);
            std::getline(ss, token, ',');
            std::getline(ss, token, ',');
            //std::cout << "t=" << t << ", token = " << token << std::endl;
            //vstrImages.push_back(strImagePath + "/" + token + ".png");
            vstrImages.push_back(p.sequence_path + "/" + token);
            std::cout << p.sequence_path + "/" + token << std::endl;
            
            vTimeStamps.push_back(t/1e9 / p.fps_multiplier);

        }
    }
}
